"""
Module: AvionController

This module contains the AvionController class, which manages the logic for interacting with aircraft data.

Classes:
    - AvionController

Functions:
    - __init__()
    - loadAvions()
    - getAvionByCode(codeAvion)
    - ajouterAvion(unAvion)
    - getLstAllAvions()

"""

import csv
from pathlib import Path

from Model.AvionModel import AvionModel


class AvionController:
    """
        Classe: AvionController

        This class manages the logic for interacting with aircraft data.

        Attributes:
            - filePath (Path): The path to the Avions.csv file
            - colonneCodeAvion (int): Index of the codeAvion column in the CSV file
            - colonneTypeAvion (int): Index of the typeAvion column in the CSV file
            - colonneModele (int): Index of the modele column in the CSV file
            - colonneNbPassagers (int): Index of the nbPassagers column in the CSV file
            - lstAllAvions (list): List containing all aircraft data

        Methods:
            - __init__(): Constructor method to initialize the AvionController class
            - loadAvions(): Method to load aircraft data from the CSV file
            - getAvionByCode(codeAvion): Method to retrieve an aircraft by its code
            - ajouterAvion(unAvion): Method to add a new aircraft to the list
            - getLstAllAvions(): Method to get the list of all aircraft
        """
    def __init__(self):
        """
        Constructor method to initialize the AvionController class.
        """
        self.filePath = Path(__file__).parent / "../Assests/Avions.csv"

        self.colonneCodeAvion = 0
        self.colonneTypeAvion = 1
        self.colonneModele = 2
        self.colonneNbPassagers = 3

        self.lstAllAvions = self.loadAvions()

    def loadAvions(self):
        """
        Method to load aircraft data from the CSV file.

        Returns:
            list: List containing all aircraft data
        """
        # Remplace le chemin par le chemin réel de ton fichier CSV
        chemin_fichier_csv = self.filePath

        result = []

        # Lire le fichier CSV
        with open(chemin_fichier_csv, 'r') as fichier_csv:
            lecteur_csv = csv.reader(fichier_csv)

            # Ignorer l'en-tête si le fichier CSV en a un
            en_tete = next(lecteur_csv, None)

            # Parcourir les lignes du fichier CSV
            for ligne in lecteur_csv:
                # Créer un objet UserModel pour chaque ligne



                avion = AvionModel(codeAvion=ligne[self.colonneCodeAvion],
                                   typeAvion=ligne[self.colonneTypeAvion],
                                   modele=ligne[self.colonneModele],
                                   nbPassagers=ligne[self.colonneNbPassagers])

                # Ajouter le billet à la liste principale
                result.append(avion)
        return result

    def getAvionByCode(self, codeAvion):
        """
         Method to retrieve an aircraft by its code.

         Parameters:
             - codeAvion (str): The code of the aircraft to retrieve

         Returns:
             AvionModel or int: An instance of AvionModel if the aircraft is found, else -1
         """
        for unAvion in self.lstAllAvions:
            if unAvion.getCodeAvion() == codeAvion:
                return unAvion
        return -1  #Indique que l'on n'a pas trouvé l'utilisateur dans la BDD

    def ajouterAvion(self, unAvion):
        """
        Method to add a new aircraft to the list.

        Parameters:
            - unAvion (AvionModel): The aircraft to add
        """
        self.lstAllAvions.append(unAvion)


    def getLstAllAvions(self):
        """
        Method to get the list of all aircraft.

        Returns:
            list: List containing all aircraft data
        """
        return self.lstAllAvions