"""
Module: AccueilController

This module contains the AccueilController class, which manages the logic for the home screen view and navigation to other views.

Classes:
    - AccueilController

Functions:
    - __init__(email)
    - update_title()
    - switch_to_accueil(instance, dt='')
    - switch_to_listeVols_passee(instance)
    - switch_to_listeVols_future(instance)
    - switch_to_creerVol(instance)
    - switch_to_listeReservation(instance)
    - switch_to_connexion(instance)

"""

from kivy.core.window import Window

from Controller.UserController import UserController
from View.Controleur.AccueilView import AccueilView
from Model.UserModel import UserModel


class AccueilController:
    """
        Classe: AccueilController

        This class manages the logic for the home screen view and navigation to other views.

        Attributes:
            - controller (UserController): Instance of the UserController class for accessing user-related data and functions
            - model (UserModel): Instance of the UserModel class for user-related operations
            - view (AccueilView): Instance of the AccueilView class for displaying the home screen
            - currentUser (UserModel): The current user

        Methods:
            - __init__(email): Constructor method to initialize the AccueilController class
            - update_title(): Method to update the title of the home screen view
            - switch_to_accueil(instance, dt=''): Method to switch to the home screen view
            - switch_to_listeVols_passee(instance): Method to switch to the past flights list view
            - switch_to_listeVols_future(instance): Method to switch to the future flights list view
            - switch_to_creerVol(instance): Method to switch to the create flight view
            - switch_to_listeReservation(instance): Method to switch to the reservations list view
            - switch_to_connexion(instance): Method to switch to the login screen view
        """
    def __init__(self, email):
        """
        Constructor method to initialize the AccueilController class.

        Parameters:
            - email (str): Email address of the user
        """
        self.controller = UserController()
        self.model = UserModel()
        self.view = AccueilView(self)
        self.currentUser = self.controller.getUtilisateurByEmail(email)
        Window.set_title("Controleur - Accueil")

        self.update_title()



    def update_title(self):
        """
        Method to update the title of the home screen view.
        """
        self.view.titre.text = "Bienvenue " + self.currentUser.getNom() + " " + self.currentUser.getPrenom() + " !"


    def switch_to_accueil(self, instance, dt=''):
        """
        Method to switch to the home screen view.

        Parameters:
            - instance: The instance triggering the event (unused)
            - dt (str): Optional parameter representing time (unused)
        """
        #Obligé de faire l'import à l'intérieur d'une méthode afin d'éviter des imports en boucles
        from Controller.Controleur.AccueilController import AccueilController
        self.accueil_controller = AccueilController(self.currentUser.getEmail())
        self.view.clear_widgets()
        self.view.add_widget(self.accueil_controller.view)


    def switch_to_listeVols_passee(self, instance):
        """
        Method to switch to the past flights list view.

        Parameters:
            - instance: The instance triggering the event (unused)
        """
        #Obligé de faire l'import à l'intérieur d'une méthode afin d'éviter des imports en boucles
        from Controller.Controleur.ListeVolsController import ListeVolsController
        self.controller_listeVols = ListeVolsController(self.currentUser.getEmail(),"passe")
        self.view.clear_widgets()
        self.view.add_widget(self.controller_listeVols.view)

    def switch_to_listeVols_future(self, instance):
        """
        Method to switch to the future flights list view.

        Parameters:
            - instance: The instance triggering the event (unused)
        """
        #Obligé de faire l'import à l'intérieur d'une méthode afin d'éviter des imports en boucles
        from Controller.Controleur.ListeVolsController import ListeVolsController
        self.controller_listeVols = ListeVolsController(self.currentUser.getEmail(),"future")
        self.view.clear_widgets()
        self.view.add_widget(self.controller_listeVols.view)



    def switch_to_creerVol(self, instance):
        """
        Method to switch to the create flight view.

        Parameters:
            - instance: The instance triggering the event (unused)
        """
        #Obligé de faire l'import à l'intérieur d'une méthode afin d'éviter des imports en boucles
        from Controller.Controleur.CreerVolController import CreerVolController
        self.controller_creerVol = CreerVolController(self.currentUser.getEmail())
        self.view.clear_widgets()
        self.view.add_widget(self.controller_creerVol.view)


    def switch_to_listeReservation(self, instance):
        """
        Method to switch to the reservations list view.

        Parameters:
            - instance: The instance triggering the event (unused)
        """
        #Obligé de faire l'import à l'intérieur d'une méthode afin d'éviter des imports en boucles
        from Controller.Controleur.ListeReservationController import ListeReservationController
        self.controller_listeReservation = ListeReservationController(self.currentUser.getEmail())
        self.view.clear_widgets()
        self.view.add_widget(self.controller_listeReservation.view)


    def switch_to_connexion(self, instance):
        """
        Method to switch to the login screen view.

        Parameters:
            - instance: The instance triggering the event (unused)
        """
        #Obligé de faire l'import à l'intérieur d'une méthode afin d'éviter des imports en boucles
        from Controller.ConnexionController import ConnexionController
        self.currentUser = ""
        self.connexion_controller = ConnexionController()
        self.view.clear_widgets()
        self.view.add_widget(self.connexion_controller.view)




