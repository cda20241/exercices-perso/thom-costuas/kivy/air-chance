"""
Module: ListeReservationController

This module contains the ListeReservationController class, which manages the logic for handling reservation lists and related views.

Classes:
    - ListeReservationController

Functions:
    - __init__(email)
    - update_title()
    - formatDonneesBillets()
    - annuler_reservation(numReservation, instance='')
    - switch_to_accueil(instance, dt='')
    - switch_to_listeVols_passee(instance)
    - switch_to_listeVols_future(instance)
    - switch_to_creerVol(instance)
    - switch_to_listeReservation(instance)
    - switch_to_connexion(instance)

"""

from kivy.clock import Clock
from kivy.core.window import Window

from Controller.UserController import UserController
from Controller.BilletController import BilletController
from View.Controleur.ListeReservationView import ListeReservationView
from Model.UserModel import UserModel
from Model.VolModel import VolModel

class ListeReservationController:
    """
    Classe: ListeReservationController

    This class manages the logic for handling reservation lists and related views.

    Attributes:
        - controller (UserController): Instance of the UserController class for accessing user-related data and functions
        - controller_billet (BilletController): Instance of the BilletController class for accessing ticket-related data and functions
        - model (UserModel): Instance of the UserModel class
        - currentUser (UserModel): The current user
        - lstData (list of dict): List of dictionaries containing reservation data
        - view (ListeReservationView): Instance of the ListeReservationView class for displaying the reservation list view

    Methods:
        - __init__(email): Constructor method to initialize the ListeReservationController class
        - update_title(): Method to update the title of the reservation list view
        - formatDonneesBillets(): Method to format reservation data
        - annuler_reservation(numReservation, instance=''): Method to cancel a reservation
        - switch_to_accueil(instance, dt=''): Method to switch to the home screen view
        - switch_to_listeVols_passee(instance): Method to switch to the past flights list view
        - switch_to_listeVols_future(instance): Method to switch to the future flights list view
        - switch_to_creerVol(instance): Method to switch to the create flight view
        - switch_to_listeReservation(instance): Method to switch to the reservations list view
        - switch_to_connexion(instance): Method to switch to the login screen view
    """
    def __init__(self, email):
        """
        Constructor method to initialize the ListeReservationController class.

        Parameters:
            - email (str): Email address of the user
        """
        self.controller = UserController()
        self.controller_billet = BilletController()

        self.model = UserModel()

        self.currentUser = self.controller.getUtilisateurByEmail(email)

        self.lstData = self.formatDonneesBillets()
        self.view = ListeReservationView(self, self.lstData)

        Window.set_title("Controleur - Liste réservations ")

        print(self.currentUser)

        self.update_title()

    def update_title(self):
        """
         Method to update the title of the reservation list view.
         """
        self.view.titre.text = "Espace liste réservations"



    def formatDonneesBillets(self):
        """
        Method to format reservation data.

        Returns:
            list of dict: List of dictionaries containing reservation data
        """
        data = []
        for unBillet in self.controller_billet.getLstAllBillets():
            print(unBillet)
            ligne = {
                "Client": unBillet.getClient(),
                "NumVol": unBillet.getVol().getNumVol(),
                "NumBillet": unBillet.getNumBillet(),
                "DateEmission": unBillet.getDateEmission(),
                "DateReservation": unBillet.getDateReservation(),
                "DatePaiement": unBillet.getDatePaiement()
            }
            data.append(ligne)
        return data

    def annuler_reservation(self, numReservation, instance=''):
        """
        Method to cancel a reservation.

        Parameters:
            - numReservation (str): Reservation number
            - instance: The instance triggering the event (unused)
        """
        # Planifier l'exécution de la suite après une pause de X secondes
        self.controller_billet.annulerBillet(numReservation)
        tempsAttenteConnexion = 2
        Clock.schedule_once(self.switch_to_accueil, tempsAttenteConnexion)



    def switch_to_accueil(self, instance, dt=''):
        """
        Method to switch to the home screen view.

        Parameters:
            - instance: The instance triggering the event (unused)
            - dt (str): Optional parameter representing time (unused)
        """
        #Obligé de faire l'import à l'intérieur d'une méthode afin d'éviter des imports en boucles
        from Controller.Controleur.AccueilController import AccueilController
        self.accueil_controller = AccueilController(self.currentUser.getEmail())
        self.view.clear_widgets()
        self.view.add_widget(self.accueil_controller.view)


    def switch_to_listeVols_passee(self, instance):
        """
        Method to switch to the past flights list view.

        Parameters:
            - instance: The instance triggering the event (unused)
        """
        #Obligé de faire l'import à l'intérieur d'une méthode afin d'éviter des imports en boucles
        from Controller.Controleur.ListeVolsController import ListeVolsController
        self.controller_listeVols = ListeVolsController(self.currentUser.getEmail(),"passe")
        self.view.clear_widgets()
        self.view.add_widget(self.controller_listeVols.view)

    def switch_to_listeVols_future(self, instance):
        """
        Method to switch to the future flights list view.

        Parameters:
            - instance: The instance triggering the event (unused)
        """
        #Obligé de faire l'import à l'intérieur d'une méthode afin d'éviter des imports en boucles
        from Controller.Controleur.ListeVolsController import ListeVolsController
        self.controller_listeVols = ListeVolsController(self.currentUser.getEmail(),"future")
        self.view.clear_widgets()
        self.view.add_widget(self.controller_listeVols.view)




    def switch_to_creerVol(self, instance):
        """
        Method to switch to the create flight view.

        Parameters:
            - instance: The instance triggering the event (unused)
        """
        #Obligé de faire l'import à l'intérieur d'une méthode afin d'éviter des imports en boucles
        from Controller.Controleur.CreerVolController import CreerVolController
        self.controller_creerVol = CreerVolController(self.currentUser.getEmail())
        self.view.clear_widgets()
        self.view.add_widget(self.controller_creerVol.view)


    def switch_to_listeReservation(self, instance):
        """
        Method to switch to the reservations list view.

        Parameters:
            - instance: The instance triggering the event (unused)
        """
        #Obligé de faire l'import à l'intérieur d'une méthode afin d'éviter des imports en boucles
        from Controller.Controleur.ListeReservationController import ListeReservationController
        self.controller_listeReservation = ListeReservationController(self.currentUser.getEmail())
        self.view.clear_widgets()
        self.view.add_widget(self.controller_listeReservation.view)


    def switch_to_connexion(self, instance):
        """
        Method to switch to the login screen view.

        Parameters:
            - instance: The instance triggering the event (unused)
        """
        #Obligé de faire l'import à l'intérieur d'une méthode afin d'éviter des imports en boucles
        from Controller.ConnexionController import ConnexionController
        self.currentUser = ""
        self.connexion_controller = ConnexionController()
        self.view.clear_widgets()
        self.view.add_widget(self.connexion_controller.view)

