"""
Module: ModifVolController

This module contains the ModifVolController class, which manages the logic for modifying flight information.

Classes:
    - ModifVolController

Functions:
    - __init__(email, numVol)
    - update_title()
    - formatDonneesVol()
    - formatDonneesAvion()
    - modifierVol(instance)
    - update_label(success)
    - switch_to_accueil(instance, dt='')
    - switch_to_listeVols_passee(instance)
    - switch_to_listeVols_future(instance)
    - switch_to_creerVol(instance)
    - switch_to_listeReservation(instance)
    - switch_to_connexion(instance)

"""

import re

from kivy.clock import Clock
from kivy.core.window import Window
from datetime import datetime, date

from Controller.UserController import UserController
from Controller.AvionController import AvionController
from Controller.VolController import VolController
from View.Controleur.ModifVolView import ModifVolView
from Model.UserModel import UserModel
from Model.AvionModel import AvionModel
from Model.VolModel import VolModel


class ModifVolController:
    """
        Classe: ModifVolController

        This class manages the logic for modifying flight information.

        Attributes:
            - controller (UserController): Instance of the UserController class for accessing user-related data and functions
            - controller_vol (VolController): Instance of the VolController class for accessing flight-related data and functions
            - controller_avion (AvionController): Instance of the AvionController class for accessing aircraft-related data and functions
            - model (UserModel): Instance of the UserModel class
            - model_vol (VolModel): Instance of the VolModel class
            - currentUser (UserModel): The current user
            - vol (VolModel): The flight to be modified
            - dataVol (dict): Dictionary containing flight data
            - dataAvion (list of dict): List of dictionaries containing aircraft data
            - view (ModifVolView): Instance of the ModifVolView class for displaying the flight modification view

        Methods:
            - __init__(email, numVol): Constructor method to initialize the ModifVolController class
            - update_title(): Method to update the title of the flight modification view
            - formatDonneesVol(): Method to format flight data
            - formatDonneesAvion(): Method to format aircraft data
            - modifierVol(instance): Method to modify the flight information
            - update_label(success): Method to update the label based on the modification success
            - switch_to_accueil(instance, dt=''): Method to switch to the home screen view
            - switch_to_listeVols_passee(instance): Method to switch to the past flights list view
            - switch_to_listeVols_future(instance): Method to switch to the future flights list view
            - switch_to_creerVol(instance): Method to switch to the create flight view
            - switch_to_listeReservation(instance): Method to switch to the reservations list view
            - switch_to_connexion(instance): Method to switch to the login screen view
        """

    def __init__(self, email, numVol):
        """
        Constructor method to initialize the ModifVolController class.

        Parameters:
            - email (str): Email address of the user
            - numVol (str): Flight number to be modified
        """
        self.controller = UserController()
        self.controller_vol = VolController()
        self.controller_avion = AvionController()

        self.model = UserModel()
        self.model_vol = VolModel()

        self.currentUser = self.controller.getUtilisateurByEmail(email)
        self.vol = self.controller_vol.getVolByNumVol(numVol)

        self.dataVol = self.formatDonneesVol()
        self.dataAvion = self.formatDonneesAvion()

        self.view = ModifVolView(self, self.dataVol, self.dataAvion)

        Window.set_title("Controleur - Modification vol ")

        print(self.currentUser)

        self.update_title()

    def update_title(self):
        """
        Method to update the title of the flight modification view.
        """
        self.view.titre.text = "Espace modification vol"

    def formatDonneesVol(self):
        """
        Method to format flight data.

        Returns:
            dict: Dictionary containing flight data
        """
        unVol = self.vol
        data = {
            "NumVol": unVol.getNumVol(),
            "DateDepart": unVol.getDateDepart(),
            "DateArrivee": unVol.getDateArrivee(),
            "HeureDepart": unVol.getHeureDepart(),
            "HeureArrivee": unVol.getHeureArrivee(),
            "VilleDepart": unVol.getVilleDepart(),
            "VilleArrivee": unVol.getVilleArrivee(),
            "Retard": unVol.getRetard(),
            "Avion": unVol.getAvion()
        }
        return data

    def formatDonneesAvion(self):
        """
        Method to format aircraft data.

        Returns:
            list of dict: List of dictionaries containing aircraft data
        """
        data=[]
        for unAvion in self.controller_avion.getLstAllAvions():
            ligne = {
                "CodeAvion": unAvion.getCodeAvion(),
                "LibelleAvion": unAvion.getTypeAvion() + " " + unAvion.getModele(),
            }
            data.append(ligne)
        return data


    def modifierVol(self, instance):
        """
        Method to modify the flight information.

        Parameters:
            - instance: The instance triggering the event
        """
        variables = {}
        for key in self.dataVol:
            variable_name = f"{key}"
            if hasattr(self.view, variable_name):
                input_widget = getattr(self.view, variable_name)
                variables[key] = input_widget.text
        variables["Avion"] = self.view.label_codeAvionSelectionne.text


        for key, value in variables.items():
            print("Key : ",key," Value : ",value)
            if value == "" or value == "-1":
                self.update_label(False)
                return None
            vol = VolModel(numvol=variables["NumVol"],
                           dateDepart=variables["DateDepart"],
                           dateArrivee=variables["DateArrivee"],
                           heureDepart=variables["HeureDepart"],
                           heureArrivee=variables["HeureArrivee"],
                           villeArrivee=variables["VilleDepart"],
                           villeDepart=variables["VilleArrivee"],
                           retard=variables["Retard"],
                           avion=variables["Avion"])

            self.controller_vol.modifierVol(self.dataVol, vol)
            self.update_label(True)
            tempsAttenteConnexion = 2
            Clock.schedule_once(self.switch_to_accueil, tempsAttenteConnexion)

    def update_label(self, success):
        """
        Method to update the label based on the modification success.

        Parameters:
            - success (bool): Indicates whether the modification was successful
        """
        if success== True:
            self.view.titre.text = ("Mofication du vol avec succès ! Redirection...")
        elif success == False:
            self.view.titre.text = "Tous les champs n'ont pas été renseignés !"
        elif success == None:
            self.view.titre.text = "Ce numéro de vol est déjà utilisé !"



    def switch_to_accueil(self, instance, dt=''):
        """
        Method to switch to the home screen view.

        Parameters:
            - instance: The instance triggering the event
            - dt (str): Optional parameter representing time (unused)
        """
        #Obligé de faire l'import à l'intérieur d'une méthode afin d'éviter des imports en boucles
        from Controller.Controleur.AccueilController import AccueilController
        self.accueil_controller = AccueilController(self.currentUser.getEmail())
        self.view.clear_widgets()
        self.view.add_widget(self.accueil_controller.view)


    def switch_to_listeVols_passee(self, instance):
        #Obligé de faire l'import à l'intérieur d'une méthode afin d'éviter des imports en boucles
        """
        Method to switch to the past flights list view.

        Parameters:
            - instance: The instance triggering the event
        """
        from Controller.Controleur.ListeVolsController import ListeVolsController
        self.controller_listeVols = ListeVolsController(self.currentUser.getEmail(),"passe")
        self.view.clear_widgets()
        self.view.add_widget(self.controller_listeVols.view)

    def switch_to_listeVols_future(self, instance):
        """
        Method to switch to the future flights list view.

        Parameters:
            - instance: The instance triggering the event
        """
        #Obligé de faire l'import à l'intérieur d'une méthode afin d'éviter des imports en boucles
        from Controller.Controleur.ListeVolsController import ListeVolsController
        self.controller_listeVols = ListeVolsController(self.currentUser.getEmail(),"future")
        self.view.clear_widgets()
        self.view.add_widget(self.controller_listeVols.view)



    def switch_to_creerVol(self, instance):
        """
        Method to switch to the create flight view.

        Parameters:
            - instance: The instance triggering the event
        """
        #Obligé de faire l'import à l'intérieur d'une méthode afin d'éviter des imports en boucles
        from Controller.Controleur.CreerVolController import CreerVolController
        self.controller_creerVol = CreerVolController(self.currentUser.getEmail())
        self.view.clear_widgets()
        self.view.add_widget(self.controller_creerVol.view)


    def switch_to_listeReservation(self, instance):
        """
        Method to switch to the reservations list view.

        Parameters:
            - instance: The instance triggering the event
        """
        #Obligé de faire l'import à l'intérieur d'une méthode afin d'éviter des imports en boucles
        from Controller.Controleur.ListeReservationController import ListeReservationController
        self.controller_listeReservation = ListeReservationController(self.currentUser.getEmail())
        self.view.clear_widgets()
        self.view.add_widget(self.controller_listeReservation.view)


    def switch_to_connexion(self, instance):
        """
        Method to switch to the login screen view.

        Parameters:
            - instance: The instance triggering the event
        """
        #Obligé de faire l'import à l'intérieur d'une méthode afin d'éviter des imports en boucles
        from Controller.ConnexionController import ConnexionController
        self.currentUser = ""
        self.connexion_controller = ConnexionController()
        self.view.clear_widgets()
        self.view.add_widget(self.connexion_controller.view)

