"""
Module: CreerVolController

This module contains the CreerVolController class, which manages the logic for creating flights and handling related views.

Classes:
    - CreerVolController

Functions:
    - __init__(email)
    - update_title()
    - formatDonneesVol()
    - formatDonneesAvion()
    - ajouterVol(instance)
    - update_label(success)
    - switch_to_accueil(instance, dt='')
    - switch_to_listeVols_passee(instance)
    - switch_to_listeVols_future(instance)
    - switch_to_creerVol(instance)
    - switch_to_listeReservation(instance)
    - switch_to_connexion(instance)

"""

from kivy.clock import Clock
from kivy.core.window import Window

from Controller.UserController import UserController
from Controller.AvionController import AvionController
from Controller.VolController import VolController
from Model.VolModel import VolModel
from View.Controleur.CreerVolView import CreerVolView
from Model.UserModel import UserModel


class CreerVolController:
    """
       Classe: CreerVolController

       This class manages the logic for creating flights and handling related views.

       Attributes:
           - controller (UserController): Instance of the UserController class for accessing user-related data and functions
           - controller_avion (AvionController): Instance of the AvionController class for accessing aircraft-related data and functions
           - controller_vol (VolController): Instance of the VolController class for accessing flight-related data and functions
           - currentUser (UserModel): The current user
           - dataVol (dict): Dictionary containing flight data
           - dataAvion (list of dict): List of dictionaries containing aircraft data
           - view (CreerVolView): Instance of the CreerVolView class for displaying the create flight view

       Methods:
           - __init__(email): Constructor method to initialize the CreerVolController class
           - update_title(): Method to update the title of the create flight view
           - formatDonneesVol(): Method to format flight data
           - formatDonneesAvion(): Method to format aircraft data
           - ajouterVol(instance): Method to add a new flight
           - update_label(success): Method to update the label text based on success status
           - switch_to_accueil(instance, dt=''): Method to switch to the home screen view
           - switch_to_listeVols_passee(instance): Method to switch to the past flights list view
           - switch_to_listeVols_future(instance): Method to switch to the future flights list view
           - switch_to_creerVol(instance): Method to switch to the create flight view
           - switch_to_listeReservation(instance): Method to switch to the reservations list view
           - switch_to_connexion(instance): Method to switch to the login screen view
       """

    def __init__(self, email):
        """
        Constructor method to initialize the CreerVolController class.

        Parameters:
            - email (str): Email address of the user
        """
        self.controller = UserController()
        self.controller_avion = AvionController()
        self.controller_vol = VolController()

        self.currentUser = self.controller.getUtilisateurByEmail(email)

        self.dataVol = self.formatDonneesVol()
        self.dataAvion = self.formatDonneesAvion()

        self.view = CreerVolView(self, self.dataVol, self.dataAvion)

        Window.set_title("Controleur - Création vol ")

        self.update_title()

    def update_title(self):
        """
        Method to update the title of the create flight view.
        """
        self.view.titre.text = "Espace Création vol"

    def formatDonneesVol(self):
        """
        Method to format flight data.

        Returns:
            dict: Dictionary containing flight data
        """
        data = {
            "NumVol": "",
            "DateDepart": "",
            "DateArrivee": "",
            "HeureDepart": "",
            "HeureArrivee": "",
            "VilleDepart": "",
            "VilleArrivee": "",
            "Retard": "",
            "Avion": ""
        }
        return data

    def formatDonneesAvion(self):
        """
        Method to format aircraft data.

        Returns:
            list of dict: List of dictionaries containing aircraft data
        """
        data=[]
        for unAvion in self.controller_avion.getLstAllAvions():
            ligne = {
                "CodeAvion": unAvion.getCodeAvion(),
                "LibelleAvion": unAvion.getTypeAvion() + " " + unAvion.getModele(),
            }
            data.append(ligne)
        return data


    def ajouterVol(self, instance):
        """
        Method to add a new flight.

        Parameters:
            - instance: The instance triggering the event (unused)
        """
        variables = {}
        for key in self.dataVol:
            variable_name = f"{key}"
            if hasattr(self.view, variable_name):
                input_widget = getattr(self.view, variable_name)
                variables[key] = input_widget.text
        variables["Avion"] = self.view.label_codeAvionSelectionne.text


        for key, value in variables.items():
            print("Key : ",key," Value : ",value)
            if value == "" or value == "-1":
                self.update_label(False)
                return None

        if self.controller_vol.getVolByNumVol(variables["NumVol"]) == -1:

            vol = VolModel(numvol=variables["NumVol"],
                           dateDepart=variables["DateDepart"],
                           dateArrivee=variables["DateArrivee"],
                           heureDepart=variables["HeureDepart"],
                           heureArrivee=variables["HeureArrivee"],
                           villeArrivee=variables["VilleDepart"],
                           villeDepart=variables["VilleArrivee"],
                           retard=variables["Retard"],
                           avion=variables["Avion"])

            self.controller_vol.ajouterVol(vol)
            self.update_label(True)
            tempsAttenteConnexion = 2
            Clock.schedule_once(self.switch_to_accueil, tempsAttenteConnexion)
        else:
            self.update_label(None)

    def update_label(self, success):
        """
        Method to update the label text based on success status.

        Parameters:
            - success: Boolean value indicating the success status
        """
        if success== True:
            self.view.titre.text = ("Ajout du vol avec succès ! Redirection...")
        elif success == False:
            self.view.titre.text = "Tous les champs n'ont pas été renseignés !"
        elif success == None:
            self.view.titre.text = "Ce numéro de vol est déjà utilisé !"

    def switch_to_accueil(self, instance, dt=''):
        """
        Method to switch to the home screen view.

        Parameters:
            - instance: The instance triggering the event (unused)
            - dt (str): Optional parameter representing time (unused)
        """
        #Obligé de faire l'import à l'intérieur d'une méthode afin d'éviter des imports en boucles
        from Controller.Controleur.AccueilController import AccueilController
        self.accueil_controller = AccueilController(self.currentUser.getEmail())
        self.view.clear_widgets()
        self.view.add_widget(self.accueil_controller.view)


    def switch_to_listeVols_passee(self, instance):
        """
        Method to switch to the past flights list view.

        Parameters:
            - instance: The instance triggering the event (unused)
        """
        #Obligé de faire l'import à l'intérieur d'une méthode afin d'éviter des imports en boucles
        from Controller.Controleur.ListeVolsController import ListeVolsController
        self.controller_listeVols = ListeVolsController(self.currentUser.getEmail(),"passe")
        self.view.clear_widgets()
        self.view.add_widget(self.controller_listeVols.view)

    def switch_to_listeVols_future(self, instance):
        """
        Method to switch to the future flights list view.

        Parameters:
            - instance: The instance triggering the event (unused)
        """
        #Obligé de faire l'import à l'intérieur d'une méthode afin d'éviter des imports en boucles
        from Controller.Controleur.ListeVolsController import ListeVolsController
        self.controller_listeVols = ListeVolsController(self.currentUser.getEmail(),"future")
        self.view.clear_widgets()
        self.view.add_widget(self.controller_listeVols.view)



    def switch_to_creerVol(self, instance):
        """
        Method to switch to the create flight view.

        Parameters:
            - instance: The instance triggering the event (unused)
        """
        #Obligé de faire l'import à l'intérieur d'une méthode afin d'éviter des imports en boucles
        from Controller.Controleur.CreerVolController import CreerVolController
        self.controller_creerVol = CreerVolController(self.currentUser.getEmail())
        self.view.clear_widgets()
        self.view.add_widget(self.controller_creerVol.view)


    def switch_to_listeReservation(self, instance):
        """
        Method to switch to the reservations list view.

        Parameters:
            - instance: The instance triggering the event (unused)
        """
        #Obligé de faire l'import à l'intérieur d'une méthode afin d'éviter des imports en boucles
        from Controller.Controleur.ListeReservationController import ListeReservationController
        self.controller_listeReservation = ListeReservationController(self.currentUser.getEmail())
        self.view.clear_widgets()
        self.view.add_widget(self.controller_listeReservation.view)


    def switch_to_connexion(self, instance):
        """
        Method to switch to the login screen view.

        Parameters:
            - instance: The instance triggering the event (unused)
        """
        #Obligé de faire l'import à l'intérieur d'une méthode afin d'éviter des imports en boucles
        from Controller.ConnexionController import ConnexionController
        self.currentUser = ""
        self.connexion_controller = ConnexionController()
        self.view.clear_widgets()
        self.view.add_widget(self.connexion_controller.view)

