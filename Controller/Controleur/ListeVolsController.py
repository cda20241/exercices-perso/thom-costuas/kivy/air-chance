"""
Module: ListeVolsController

This module contains the ListeVolsController class, which manages the logic for handling flight lists and related views.

Classes:
    - ListeVolsController

Functions:
    - __init__(email, periode)
    - update_title()
    - formatDonneesVol()
    - trieParPeriode(vol)
    - trieParFiltre(data)
    - change_filter(instance)
    - effacerTableau()
    - annuler_vol(numVol, instance='')
    - switch_to_modif(numVol)
    - switch_to_accueil(instance, dt='')
    - switch_to_listeVols_passee(instance)
    - switch_to_listeVols_future(instance)
    - switch_to_creerVol(instance)
    - switch_to_listeReservation(instance)
    - switch_to_connexion(instance)

"""

import re

from kivy.clock import Clock
from kivy.core.window import Window
from datetime import datetime, date

from Controller.UserController import UserController
from Controller.AvionController import AvionController
from Controller.VolController import VolController
from View.Controleur.ListeVolsView import ListeVolsView
from Model.UserModel import UserModel
from Model.AvionModel import AvionModel
from Model.VolModel import VolModel


class ListeVolsController:
    """
       Classe: ListeVolsController

       This class manages the logic for handling flight lists and related views.

       Attributes:
           - controller (UserController): Instance of the UserController class for accessing user-related data and functions
           - controller_vol (VolController): Instance of the VolController class for accessing flight-related data and functions
           - controller_avion (AvionController): Instance of the AvionController class for accessing aircraft-related data and functions
           - model (UserModel): Instance of the UserModel class
           - model_vol (VolModel): Instance of the VolModel class
           - currentUser (UserModel): The current user
           - periode (str): Indicates whether the flight list is for past or future flights
           - filtreIndex (int): Index used for filtering flights
           - filterList (list of str): List of filter options
           - filtre (str): Current filter option
           - lstData (list of dict): List of dictionaries containing flight data
           - view (ListeVolsView): Instance of the ListeVolsView class for displaying the flight list view

       Methods:
           - __init__(email, periode): Constructor method to initialize the ListeVolsController class
           - update_title(): Method to update the title of the flight list view
           - formatDonneesVol(): Method to format flight data
           - trieParPeriode(vol): Method to filter flights based on past or future criteria
           - trieParFiltre(data): Method to sort flight data based on a filter
           - change_filter(instance): Method to change the filter option
           - effacerTableau(): Method to clear the flight list table
           - annuler_vol(numVol, instance=''): Method to cancel a flight
           - switch_to_modif(numVol): Method to switch to the flight modification view
           - switch_to_accueil(instance, dt=''): Method to switch to the home screen view
           - switch_to_listeVols_passee(instance): Method to switch to the past flights list view
           - switch_to_listeVols_future(instance): Method to switch to the future flights list view
           - switch_to_creerVol(instance): Method to switch to the create flight view
           - switch_to_listeReservation(instance): Method to switch to the reservations list view
           - switch_to_connexion(instance): Method to switch to the login screen view
       """
    def __init__(self, email, periode):
        """
        Constructor method to initialize the ListeVolsController class.

        Parameters:
            - email (str): Email address of the user
            - periode (str): Indicates whether the flight list is for past or future flights
        """
        self.controller = UserController()
        self.controller_vol = VolController()
        self.controller_avion = AvionController()

        self.model = UserModel()
        self.model_vol = VolModel()

        self.currentUser = self.controller.getUtilisateurByEmail(email)
        self.periode = periode

        self.filtreIndex = 0
        self.filterList = ['-','< ','>']
        self.filtre = self.filterList[self.filtreIndex]

        self.lstData = self.formatDonneesVol()
        self.view = ListeVolsView(self, self.lstData, self.filtre)
        self.view.creerTableau(self.lstData)

        Window.set_title("Controleur - Liste vols ")

        print(self.currentUser)

        self.update_title()

    def update_title(self):
        """
        Method to update the title of the flight list view.
        """
        self.view.titre.text = "Espace liste vols"

    def formatDonneesVol(self):
        """
        Method to format flight data.

        Returns:
            list of dict: List of dictionaries containing flight data
        """
        data = []
        for unVol in self.controller_vol.getLstAllVols():
            if self.trieParPeriode(unVol):
                avion = self.controller_avion.getAvionByCode(unVol.getAvion())
                ligne = {
                    "NumVol": unVol.getNumVol(),
                    "DateDepart": unVol.getDateDepart(),
                    "DateArrivee": unVol.getDateArrivee(),
                    "HeureDepart": unVol.getHeureDepart(),
                    "HeureArrivee": unVol.getHeureArrivee(),
                    "VilleDepart": unVol.getVilleDepart(),
                    "VilleArrivee": unVol.getVilleArrivee(),
                    "Retard": unVol.getRetard(),
                    "Avion": avion.getTypeAvion()+'\n'+avion.getModele(),
                }
                data.append(ligne)
        return data



    def trieParPeriode(self, vol):
        """
        Method to filter flights based on past or future criteria.

        Parameters:
            - vol (VolModel): Instance of the VolModel class representing a flight

        Returns:
            bool: True if the flight meets the criteria, False otherwise
        """
        dateDepart = datetime.strptime(vol.getDateDepart(), '%Y-%m-%d').date()
        dateActuelle = date.today()
        if self.periode == "passe":
            return dateDepart < dateActuelle
        elif self.periode == "future":
            return dateDepart > dateActuelle



    def trieParFiltre(self, data):
        """
        Method to sort flight data based on a filter.

        Parameters:
            - data (list of dict): List of dictionaries containing flight data

        Returns:
            list of dict: Sorted list of dictionaries containing flight data
        """
        if self.filtre == '-':
            data = sorted(data, key=lambda x: x["DateDepart"])
        elif self.filtre == '<':
            data = sorted(data, key=lambda x: x["Avion"])  # Tri par le nom de l'avion
        elif self.filtre == '>':
            data = sorted(data, key=lambda x: x["Avion"], reverse=True)
        return data



    def change_filter(self, instance):
        """
        Method to change the filter option.

        Parameters:
            - instance: The instance triggering the event
        """
        self.filtreIndex = (self.filtreIndex + 1) % len(self.filterList)
        self.filtre = self.filterList[self.filtreIndex]  # Update filtre based on filterList
        self.lstData = self.trieParFiltre(self.lstData)

        for unHeader in self.view.header_row.children:
            if "Avion" in unHeader.text:
                unHeader.text = "Avion "+self.filtre

        self.effacerTableau()
        self.view.creerTableau(self.lstData)



    def effacerTableau(self):
        """
        Method to clear the flight list table.
        """
        # Determine the number of data_row widgets dynamically
        num_data_rows = len(self.lstData)  # Assuming data is a list of dictionaries
        # Check if the variables are created
        for j in range(num_data_rows):
            variable_name = f"data_row_{j}"
            if hasattr(self.view, variable_name):
                data_row_widget = getattr(self.view, variable_name)
                self.view.remove_widget(data_row_widget)
        self.view.remove_widget(self.view.espace)


    def annuler_vol(self, numVol, instance=''):
        """
        Method to cancel a flight.

        Parameters:
            - numVol (str): Flight number
            - instance: The instance triggering the event (unused)
        """
        self.controller_vol.annulerVol(numVol)
        # Planifier l'exécution de la suite après une pause de X secondes
        # tempsAttenteConnexion = 2
        tempsAttenteConnexion = 2
        Clock.schedule_once(self.switch_to_accueil, tempsAttenteConnexion)


    def switch_to_modif(self, numVol):
        """
        Method to switch to the flight modification view.

        Parameters:
            - numVol (str): Flight number
        """
        #Obligé de faire l'import à l'intérieur d'une méthode afin d'éviter des imports en boucles
        from Controller.Controleur.ModifVolController import ModifVolController
        self.controller_modif = ModifVolController(self.currentUser.getEmail(), numVol)
        self.view.clear_widgets()
        self.view.add_widget(self.controller_modif.view)


    def switch_to_accueil(self, instance, dt=''):
        """
        Method to switch to the home screen view.

        Parameters:
            - instance: The instance triggering the event
            - dt (str): Optional parameter representing time (unused)
        """
        #Obligé de faire l'import à l'intérieur d'une méthode afin d'éviter des imports en boucles
        from Controller.Controleur.AccueilController import AccueilController
        self.accueil_controller = AccueilController(self.currentUser.getEmail())
        self.view.clear_widgets()
        self.view.add_widget(self.accueil_controller.view)


    def switch_to_listeVols_passee(self, instance):
        """
        Method to switch to the past flights list view.

        Parameters:
            - instance: The instance triggering the event
        """

        #Obligé de faire l'import à l'intérieur d'une méthode afin d'éviter des imports en boucles
        from Controller.Controleur.ListeVolsController import ListeVolsController
        self.controller_listeVols = ListeVolsController(self.currentUser.getEmail(),"passe")
        self.view.clear_widgets()
        self.view.add_widget(self.controller_listeVols.view)

    def switch_to_listeVols_future(self, instance):
        """
        Method to switch to the future flights list view.

        Parameters:
            - instance: The instance triggering the event
        """
        #Obligé de faire l'import à l'intérieur d'une méthode afin d'éviter des imports en boucles
        from Controller.Controleur.ListeVolsController import ListeVolsController
        self.controller_listeVols = ListeVolsController(self.currentUser.getEmail(),"future")
        self.view.clear_widgets()
        self.view.add_widget(self.controller_listeVols.view)




    def switch_to_creerVol(self, instance):
        """
        Method to switch to the create flight view.

        Parameters:
            - instance: The instance triggering the event
        """
        #Obligé de faire l'import à l'intérieur d'une méthode afin d'éviter des imports en boucles
        from Controller.Controleur.CreerVolController import CreerVolController
        self.controller_creerVol = CreerVolController(self.currentUser.getEmail())
        self.view.clear_widgets()
        self.view.add_widget(self.controller_creerVol.view)


    def switch_to_listeReservation(self, instance):
        """
        Method to switch to the reservations list view.

        Parameters:
            - instance: The instance triggering the event
        """
        #Obligé de faire l'import à l'intérieur d'une méthode afin d'éviter des imports en boucles
        from Controller.Controleur.ListeReservationController import ListeReservationController
        self.controller_listeReservation = ListeReservationController(self.currentUser.getEmail())
        self.view.clear_widgets()
        self.view.add_widget(self.controller_listeReservation.view)


    def switch_to_connexion(self, instance):
        """
        Method to switch to the login screen view.

        Parameters:
            - instance: The instance triggering the event
        """
        #Obligé de faire l'import à l'intérieur d'une méthode afin d'éviter des imports en boucles
        from Controller.ConnexionController import ConnexionController
        self.currentUser = ""
        self.connexion_controller = ConnexionController()
        self.view.clear_widgets()
        self.view.add_widget(self.connexion_controller.view)

