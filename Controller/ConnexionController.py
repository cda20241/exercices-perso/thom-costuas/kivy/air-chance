"""
Module: ConnexionController

This module contains the ConnexionController class, which manages the logic for user authentication and navigation.

Author: [Author Name]

Date: [Date]

Classes:
    - ConnexionController

Functions:
    - __init__()
    - valider_connexion(email, mdp)
    - on_submit(instance)
    - update_label(success)
    - switch_to_inscription(instance)
    - switch_to_accueil(instance)
    - switch_to_accueil_controller(instance, dt='')

"""

from kivy.clock import Clock
from kivy.core.window import Window

from Controller.UserController import UserController
from View.ConnexionView import ConnexionView
from Model.UserModel import UserModel


class ConnexionController:
    """
    Classe: ConnexionController

    This class manages the logic for user authentication and navigation.

    Attributes:
        - controller (UserController): Instance of UserController for managing user data
        - model (UserModel): Instance of UserModel for user data modeling
        - view (ConnexionView): Instance of ConnexionView for user interface
        - currentUser (str): Current user logged in
    """
    def __init__(self):
        """
        Constructor method to initialize the ConnexionController class.
        """
        self.controller = UserController()
        self.model = UserModel()
        self.view = ConnexionView(self)
        self.currentUser = ""
        Window.set_title("Connexion")




    def valider_connexion(self, email, mdp):
        """
        Method to validate user authentication.

        Parameters:
            - email (str): User's email
            - mdp (str): User's password
        """
        if self.controller.identifiantsValide(email, mdp):
            self.currentUser = self.controller.getUtilisateurByEmail(email)
            self.update_label(True)

            # Planifier l'exécution de la suite après une pause de X secondes
            tempsAttenteConnexion = 2
            if self.currentUser.getStatut() == "Client":
                Clock.schedule_once(self.switch_to_accueil, tempsAttenteConnexion)
            elif self.currentUser.getStatut() == "Controleur":
                Clock.schedule_once(self.switch_to_accueil_controller, tempsAttenteConnexion)
        else:
            self.update_label(False)


    def on_submit(self, instance):
        """
        Method called when the user submits login credentials.

        Parameters:
            - instance: The instance triggering the event
        """
        self.valider_connexion(self.view.user_input.text, self.view.password_input.text)


    def update_label(self, success):
        """
        Method to update the label text based on authentication success or failure.

        Parameters:
            - success (bool): Boolean indicating if authentication was successful
        """
        if success:
            self.view.label.text = "Bienvenue " + self.currentUser.getNom() + " " + self.currentUser.getPrenom() + " !"
        else:
            self.view.label.text = "Identifiant ou mot de passe incorrect !"



    def switch_to_inscription(self, instance):
        """
        Method to switch to the registration view.

        Parameters:
            - instance: The instance triggering the event
        """
        #Obligé de faire l'import à l'intérieur d'une méthode afin d'éviter des imports en boucles
        from Controller.InscriptionController import InscriptionController
        self.inscription_controller = InscriptionController()
        self.view.clear_widgets()
        self.view.add_widget(self.inscription_controller.view)


    # dt est un parametre auto fournie par Clock, obligé de le spécifier en param sinon erreur
    def switch_to_accueil(self, instance):
        """
        Method to switch to the home view for clients.

        Parameters:
            - instance: The instance triggering the event
        """
        # Obligé de faire l'import à l'intérieur d'une méthode afin d'éviter des imports en boucles
        from Controller.Client.AccueilController import AccueilController
        self.accueil_controller = AccueilController(self.currentUser.getEmail())
        self.view.clear_widgets()
        self.view.add_widget(self.accueil_controller.view)


    def switch_to_accueil_controller(self, instance, dt=''):
        """
        Method to switch to the home view for controllers.

        Parameters:
            - instance: The instance triggering the event
            - dt (str): Optional parameter provided by Clock (default: '')
        """
        # Obligé de faire l'import à l'intérieur d'une méthode afin d'éviter des imports en boucles
        from Controller.Controleur.AccueilController import AccueilController
        self.accueil_controller = AccueilController(self.currentUser.getEmail())
        self.view.clear_widgets()
        self.view.add_widget(self.accueil_controller.view)
