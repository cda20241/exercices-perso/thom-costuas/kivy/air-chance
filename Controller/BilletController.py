"""
Module: BilletController

This module contains the BilletController class, which manages the logic for interacting with ticket data.

Classes:
    - BilletController

Functions:
    - __init__()
    - loadBillets()
    - lstBilletsByEmail(email)
    - ajouterBillet(unBillet)
    - modifierBillet(oldCodeVol, newCodeVol)
    - annulerBillet(codeVol)
    - getLstAllBillets()
    - getLastBillet()

"""

import csv
from pathlib import Path

from Controller.VolController import VolController
from Model.BilletModel import BilletModel


class BilletController:
    """
        Classe: BilletController

        This class manages the logic for interacting with ticket data.

        Attributes:
            - filePath (Path): The path to the Billets.csv file
            - controller_vol (VolController): Instance of VolController for managing flight data
            - colonneEmail (int): Index of the email column in the CSV file
            - colonneVol (int): Index of the vol column in the CSV file
            - colonneNumero (int): Index of the numero column in the CSV file
            - colonneDateEmission (int): Index of the dateEmission column in the CSV file
            - colonneDateReservation (int): Index of the dateReservation column in the CSV file
            - colonneDatePaiement (int): Index of the datePaiement column in the CSV file
            - lstAllBillets (list): List containing all ticket data

        Methods:
            - __init__(): Constructor method to initialize the BilletController class
            - loadBillets(): Method to load ticket data from the CSV file
            - lstBilletsByEmail(email): Method to get a list of tickets by email
            - ajouterBillet(unBillet): Method to add a new ticket to the list and CSV file
            - modifierBillet(oldCodeVol, newCodeVol): Method to modify a ticket's flight code
            - annulerBillet(codeVol): Method to cancel a ticket based on flight code
            - getLstAllBillets(): Method to get the list of all tickets
            - getLastBillet(): Method to get the last added ticket
        """
    def __init__(self):
        """
        Constructor method to initialize the BilletController class.
        """
        self.filePath = Path(__file__).parent / "../Assests/Billets.csv"

        self.controller_vol = VolController()


        self.colonneEmail = 0
        self.colonneVol = 1
        self.colonneNumero = 2
        self.colonneDateEmission = 3
        self.colonneDateReservation = 4
        self.colonneDatePaiement = 5

        self.lstAllBillets = self.loadBillets()

    def loadBillets(self):
        """
        Method to load ticket data from the CSV file.

        Returns:
            list: List containing all ticket data
        """
        # Remplace le chemin par le chemin réel de ton fichier CSV
        chemin_fichier_csv = self.filePath

        result = []

        # Lire le fichier CSV
        with open(chemin_fichier_csv, 'r') as fichier_csv:
            lecteur_csv = csv.reader(fichier_csv)

            # Ignorer l'en-tête si le fichier CSV en a un
            en_tete = next(lecteur_csv, None)

            # Parcourir les lignes du fichier CSV
            for ligne in lecteur_csv:
                # Créer un objet UserModel pour chaque ligne

                unClient = ligne[self.colonneEmail]
                unVol = self.controller_vol.getVolByNumVol(ligne[self.colonneVol])


                billet = BilletModel(client=unClient,
                                    vol=unVol,
                                    numBillet=ligne[self.colonneNumero],
                                    dateEmission=ligne[self.colonneDateEmission],
                                    dateReservation=ligne[self.colonneDateReservation],
                                    datePaiement=ligne[self.colonneDatePaiement])

                # Ajouter le billet à la liste principale
                result.append(billet)
        return result

    def lstBilletsByEmail(self, email):
        """
        Method to get a list of tickets by email.

        Parameters:
            - email (str): The email of the user to filter tickets

        Returns:
            list: List containing tickets for the specified email
        """
        result =[]
        for unBillet in self.lstAllBillets:
            if unBillet.getClient() == email:
                result.append(unBillet)
        return result



    def ajouterBillet(self, unBillet):
        """
        Method to add a new ticket to the list and CSV file.

        Parameters:
            - unBillet (BilletModel): The ticket to add
        """
        # Append the new ticket to the list
        self.lstAllBillets.append(unBillet)

        # Write the new ticket to the CSV file
        with open(self.filePath, 'a', newline='') as fichier_csv:
            writer = csv.writer(fichier_csv)

            # Write a new row for the added ticket
            writer.writerow([
                unBillet.getClient(),
                unBillet.getVol(),
                unBillet.getNumBillet(),
                unBillet.getDateEmission(),
                unBillet.getDateReservation(),
                unBillet.getDatePaiement()
            ])

    def modifierBillet(self, oldCodeVol, newCodeVol):
        """
        Method to modify a ticket's flight code.

        Parameters:
            - oldCodeVol (str): The old flight code of the ticket
            - newCodeVol (str): The new flight code to replace the old one
        """
        # Charger toutes les lignes existantes du fichier CSV
        with open(self.filePath, 'r', newline='') as csv_file:
            csv_reader = csv.reader(csv_file)
            rows = list(csv_reader)

        # Trouver la ligne à modifier en utilisant le numéro de vol
        for index, row in enumerate(rows):
            if row[self.colonneVol] == oldCodeVol:
                # Modifier les données de cette ligne
                rows[index] = [
                    row[self.colonneEmail],
                    newCodeVol,
                    row[self.colonneNumero],
                    row[self.colonneDateEmission],
                    row[self.colonneDateReservation],
                    row[self.colonneDatePaiement],
                ]
                break

        # Réécrire toutes les lignes dans le fichier CSV
        with open(self.filePath, 'w', newline='') as csv_file:
            csv_writer = csv.writer(csv_file)
            csv_writer.writerows(rows)


    def annulerBillet(self, codeVol):
        """
        Method to cancel a ticket based on flight code.

        Parameters:
            - codeVol (str): The flight code of the ticket to cancel
        """
        # Charger toutes les lignes existantes du fichier CSV
        with open(self.filePath, 'r', newline='') as fichier_csv:
            lecteur_csv = csv.reader(fichier_csv)
            lignes = list(lecteur_csv)

        # Créer une nouvelle liste de lignes sans la ligne à supprimer
        nouvelles_lignes = [ligne for ligne in lignes if ligne[self.colonneVol] != codeVol]

        # Réécrire toutes les lignes dans le fichier CSV
        with open(self.filePath, 'w', newline='') as fichier_csv:
            writer = csv.writer(fichier_csv)
            writer.writerows(nouvelles_lignes)


    def getLstAllBillets(self):
        """
        Method to get the list of all tickets.

        Returns:
            list: List containing all ticket data
        """
        return self.lstAllBillets

    def getLastBillet(self):
        """
        Method to get the last added ticket.

        Returns:
            BilletModel: The last added ticket
        """
        return self.lstAllBillets[-1]
