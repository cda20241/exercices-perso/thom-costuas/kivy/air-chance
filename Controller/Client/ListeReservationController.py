"""
Module: ListeReservationController

This module contains the ListeReservationController class, which handles the logic for displaying and managing reservations
for a specific user, based on the selected period (past or future).


Classes:
    - ListeReservationController

Functions:
    - __init__(email, periode)
    - update_title()
    - formatDonneesReservation()
    - trieParPeriode(vol)
    - switch_to_accueil(instance, dt='')
    - switch_to_listeReservations_passee(instance)
    - switch_to_listeReservations_future(instance)
    - switch_to_listeVols(instance)
    - switch_to_connexion(instance)

"""

from kivy.core.window import Window
from datetime import date, datetime

from Controller.UserController import UserController
from View.Client.ListeReservationView import ListeReservationView
from Model.UserModel import UserModel
from Model.VolModel import VolModel


class ListeReservationController:
    """
        Classe: ListeReservationController

        This class manages the logic for displaying and managing reservations for a specific user, based on the selected period
        (past or future).

        Attributes:
            - periode (str): The selected period ('passee' for past reservations, 'future' for future reservations)
            - controller (UserController): Instance of the UserController class for accessing user-related data and functions
            - model (UserModel): Instance of the UserModel class for user-related operations
            - model_vol (VolModel): Instance of the VolModel class for flight-related operations
            - currentUser (UserModel): The current user for whom reservations are being displayed
            - lstData (list): List containing formatted reservation data to be displayed
            - view (ListeReservationView): Instance of the ListeReservationView class for displaying reservation data

        Methods:
            - __init__(email, periode): Constructor method to initialize the ListeReservationController class
            - update_title(): Method to update the title of the reservation list view
            - formatDonneesReservation(): Method to format reservation data based on the selected period
            - trieParPeriode(vol): Method to determine if a flight falls within the selected period
            - switch_to_accueil(instance, dt=''): Method to switch to the home screen view
            - switch_to_listeReservations_passee(instance): Method to switch to the past reservations list view
            - switch_to_listeReservations_future(instance): Method to switch to the future reservations list view
            - switch_to_listeVols(instance): Method to switch to the flight list view
            - switch_to_connexion(instance): Method to switch to the login screen view
        """

    def __init__(self, email, periode):
        """
        Constructor method to initialize the ListeReservationController class.

        Parameters:
            - email (str): Email address of the user
            - periode (str): The selected period ('passee' for past reservations, 'future' for future reservations)
        """
        self.periode=periode
        self.controller = UserController()
        self.model = UserModel()
        self.model_vol = VolModel()

        self.currentUser = self.controller.getUtilisateurByEmail(email)

        self.lstData = self.formatDonneesReservation()
        self.view = ListeReservationView(self, self.lstData)

        Window.set_title("Liste Réservations "+self.periode)
        print(self.currentUser)
        self.update_title()

    def update_title(self):
        """
        Method to update the title of the reservation list view.
        """
        self.view.titre.text = "Espace liste reservations "+self.periode+"s"



    def formatDonneesReservation(self):
        """
        Method to format reservation data based on the selected period.

        Returns:
            list: List containing formatted reservation data
        """
        data = []
        for unBillet in self.currentUser.getLstBillets():
            if self.trieParPeriode(unBillet.getVol()):
                ligne = {
                    "Client": unBillet.getClient(),
                    "NumVol": unBillet.getVol().getNumVol(),
                    "NumBillet": unBillet.getNumBillet(),
                    "DateEmission": unBillet.getDateEmission(),
                    "DateReservation": unBillet.getDateReservation(),
                    "DatePaiement": unBillet.getDatePaiement()
                }
                data.append(ligne)
        return data



    def trieParPeriode(self, vol):
        """
        Method to determine if a flight falls within the selected period.

        Parameters:
            - vol (VolModel): Instance of the VolModel class representing a flight

        Returns:
            bool: True if the flight falls within the selected period, False otherwise
        """
        dateDepart = datetime.strptime(vol.getDateDepart(), '%Y-%m-%d').date()
        dateActuelle = date.today()
        if self.periode == "passee":
            if dateDepart < dateActuelle:
                return True
            return False
        elif self.periode == "future":
            if dateDepart > dateActuelle:
                return True
            return False


    def switch_to_accueil(self, instance, dt=''):
        """
        Method to switch to the home screen view.

        Parameters:
            - instance: The instance triggering the event (unused)
            - dt (str): Optional parameter representing time (unused)
        """
        #Obligé de faire l'import à l'intérieur d'une méthode afin d'éviter des imports en boucles
        from Controller.Client.AccueilController import AccueilController
        self.accueil_controller = AccueilController(self.currentUser.getEmail())
        self.view.clear_widgets()
        self.view.add_widget(self.accueil_controller.view)



    def switch_to_listeReservations_passee(self, instance):
        """
        Method to switch to the past reservations list view.

        Parameters:
            - instance: The instance triggering the event (unused)
        """
        #Obligé de faire l'import à l'intérieur d'une méthode afin d'éviter des imports en boucles
        from Controller.Client.ListeReservationController import ListeReservationController
        self.listereservation_controller = ListeReservationController(self.currentUser.getEmail(),"passee")
        self.view.clear_widgets()
        self.view.add_widget(self.listereservation_controller.view)



    def switch_to_listeReservations_future(self, instance):
        """
        Method to switch to the future reservations list view.

        Parameters:
            - instance: The instance triggering the event (unused)
        """
        #Obligé de faire l'import à l'intérieur d'une méthode afin d'éviter des imports en boucles
        from Controller.Client.ListeReservationController import ListeReservationController
        self.listereservation_controller = ListeReservationController(self.currentUser.getEmail(), "future")
        self.view.clear_widgets()
        self.view.add_widget(self.listereservation_controller.view)



    def switch_to_listeVols(self, instance):
        """
        Method to switch to the flight list view.

        Parameters:
            - instance: The instance triggering the event (unused)
        """
        #Obligé de faire l'import à l'intérieur d'une méthode afin d'éviter des imports en boucles
        from Controller.Client.ListeVolsController import ListeVolsController
        self.listevols_controller = ListeVolsController(self.currentUser.getEmail())
        self.view.clear_widgets()
        self.view.add_widget(self.listevols_controller.view)


    def switch_to_connexion(self, instance):
        """
        Method to switch to the login screen view.

        Parameters:
            - instance: The instance triggering the event (unused)
        """
        #Obligé de faire l'import à l'intérieur d'une méthode afin d'éviter des imports en boucles
        from Controller.ConnexionController import ConnexionController
        self.currentUser = ""
        self.connexion_controller = ConnexionController()
        self.view.clear_widgets()
        self.view.add_widget(self.connexion_controller.view)