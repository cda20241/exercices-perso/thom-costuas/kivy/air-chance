"""
Module: ListeVolsController

This module contains the ListeVolsController class, which manages the logic for displaying and managing flight listings for a specific user.

Classes:
    - ListeVolsController

Functions:
    - __init__(email)
    - update_title()
    - formatDonneesReservation()
    - trieAVenir(vol)
    - switch_to_accueil(instance, dt='')
    - switch_to_listeReservations_passee(instance)
    - switch_to_listeReservations_future(instance)
    - switch_to_listeVols(instance)
    - switch_to_connexion(instance)
    - switch_to_ville(instance)
    - switch_to_reservation(instance)

"""

import re

from kivy.core.window import Window
from datetime import datetime, date

from Controller.UserController import UserController
from Controller.VolController import VolController
from View.Client.ListeVolsView import ListeVolsView
from Model.VolModel import VolModel
from Model.UserModel import UserModel


class ListeVolsController:
    """
        Classe: ListeVolsController

        This class manages the logic for displaying and managing flight listings for a specific user.

        Attributes:
            - controller (UserController): Instance of the UserController class for accessing user-related data and functions
            - controller_vol (VolController): Instance of the VolController class for accessing flight-related data and functions
            - model (UserModel): Instance of the UserModel class for user-related operations
            - model_vol (VolModel): Instance of the VolModel class for flight-related operations
            - currentUser (UserModel): The current user for whom flight listings are being displayed
            - lstData (list): List containing formatted flight data to be displayed
            - view (ListeVolsView): Instance of the ListeVolsView class for displaying flight data

        Methods:
            - __init__(email): Constructor method to initialize the ListeVolsController class
            - update_title(): Method to update the title of the flight list view
            - formatDonneesReservation(): Method to format flight data for display
            - trieAVenir(vol): Method to determine if a flight is in the future
            - switch_to_accueil(instance, dt=''): Method to switch to the home screen view
            - switch_to_listeReservations_passee(instance): Method to switch to the past reservations list view
            - switch_to_listeReservations_future(instance): Method to switch to the future reservations list view
            - switch_to_listeVols(instance): Method to switch to the flight list view
            - switch_to_connexion(instance): Method to switch to the login screen view
            - switch_to_ville(instance): Method to switch to the city view
            - switch_to_reservation(instance): Method to switch to the reservation view
        """
    def __init__(self, email):
        """
        Constructor method to initialize the ListeVolsController class.

        Parameters:
            - email (str): Email address of the user
        """
        self.controller = UserController()
        self.controller_vol = VolController()

        self.model = UserModel()
        self.model_vol = VolModel()

        self.currentUser = self.controller.getUtilisateurByEmail(email)

        self.lstData = self.formatDonneesReservation()
        self.view = ListeVolsView(self, self.lstData)

        Window.set_title("Liste vols ")
        print(self.currentUser)
        self.update_title()

    def update_title(self):
        """
        Method to update the title of the flight list view.
        """
        self.view.titre.text = "Espace liste vols"



    def formatDonneesReservation(self):
        """
        Method to format flight data for display.

        Returns:
            list: List containing formatted flight data
        """
        data = []
        for unVol in self.controller_vol.getLstAllVols():
            if self.trieAVenir(unVol):
                ligne = {
                    "NumVol": unVol.getNumVol(),
                    "DateDepart": unVol.getDateDepart(),
                    "DateArrivee": unVol.getDateArrivee(),
                    "HeureDepart": unVol.getHeureDepart(),
                    "HeureArrivee": unVol.getHeureArrivee(),
                    "VilleDepart": unVol.getVilleDepart(),
                    "VilleArrivee": unVol.getVilleArrivee(),
                    "Retard": unVol.getRetard(),
                    "Avion": unVol.getAvion()
                }
                data.append(ligne)
        return data



    def trieAVenir(self, vol):
        """
        Method to determine if a flight is in the future.

        Parameters:
            - vol (VolModel): Instance of the VolModel class representing a flight

        Returns:
            bool: True if the flight is in the future, False otherwise
        """
        dateDepart = datetime.strptime(vol.getDateDepart(), '%Y-%m-%d').date()
        dateActuelle = date.today()
        if dateDepart > dateActuelle:
            return True
        return False



    def switch_to_accueil(self, instance, dt=''):
        """
        Method to switch to the home screen view.

        Parameters:
            - instance: The instance triggering the event (unused)
            - dt (str): Optional parameter representing time (unused)
        """
        # Obligé de faire l'import à l'intérieur d'une méthode afin d'éviter des imports en boucles
        from Controller.Client.AccueilController import AccueilController
        self.accueil_controller = AccueilController(self.currentUser.getEmail())
        self.view.clear_widgets()
        self.view.add_widget(self.accueil_controller.view)



    def switch_to_listeReservations_passee(self, instance):
        """
        Method to switch to the past reservations list view.

        Parameters:
            - instance: The instance triggering the event (unused)
        """
        # Obligé de faire l'import à l'intérieur d'une méthode afin d'éviter des imports en boucles
        from Controller.Client.ListeReservationController import ListeReservationController
        self.listereservation_controller = ListeReservationController(self.currentUser.getEmail(), "passee")
        self.view.clear_widgets()
        self.view.add_widget(self.listereservation_controller.view)



    def switch_to_listeReservations_future(self, instance):
        """
        Method to switch to the future reservations list view.

        Parameters:
            - instance: The instance triggering the event (unused)
        """
        # Obligé de faire l'import à l'intérieur d'une méthode afin d'éviter des imports en boucles
        from Controller.Client.ListeReservationController import ListeReservationController
        self.listereservation_controller = ListeReservationController(self.currentUser.getEmail(), "future")
        self.view.clear_widgets()
        self.view.add_widget(self.listereservation_controller.view)



    def switch_to_listeVols(self, instance):
        """
        Method to switch to the flight list view.

        Parameters:
            - instance: The instance triggering the event (unused)
        """
        # Obligé de faire l'import à l'intérieur d'une méthode afin d'éviter des imports en boucles
        from Controller.Client.ListeVolsController import ListeVolsController
        self.listevols_controller = ListeVolsController(self.currentUser.getEmail())
        self.view.clear_widgets()
        self.view.add_widget(self.listevols_controller.view)



    def switch_to_connexion(self, instance):
        """
        Method to switch to the login screen view.

        Parameters:
            - instance: The instance triggering the event (unused)
        """
        # Obligé de faire l'import à l'intérieur d'une méthode afin d'éviter des imports en boucles
        from Controller.ConnexionController import ConnexionController
        self.currentUser = ""
        self.connexion_controller = ConnexionController()
        self.view.clear_widgets()
        self.view.add_widget(self.connexion_controller.view)


    def switch_to_ville(self, instance):
        """
        Method to switch to the city view.

        Parameters:
            - instance: The instance triggering the event
        """
        # Obligé de faire l'import à l'intérieur d'une méthode afin d'éviter des imports en boucles
        from Controller.Client.VilleController import VilleController
        self.ville = re.sub(r'\[.*?\]', '', instance.text)
        self.ville_controller = VilleController(self.currentUser.getEmail(), self.ville)
        self.view.clear_widgets()
        self.view.add_widget(self.ville_controller.view)


    def switch_to_reservation(self, instance):
        """
        Method to switch to the city view.

        Parameters:
            - instance: The instance triggering the event
        """
        # Obligé de faire l'import à l'intérieur d'une méthode afin d'éviter des imports en boucles
        from Controller.Client.ReservationController import ReservationController
        self.vol = re.sub(r'\[.*?\]', '', instance.text)
        self.controller_reservation = ReservationController(self.currentUser.getEmail(), self.vol)
        self.view.clear_widgets()
        self.view.add_widget(self.controller_reservation.view)


