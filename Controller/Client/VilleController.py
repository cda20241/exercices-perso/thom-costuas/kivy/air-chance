"""
Module: VilleController

This module contains the VilleController class, which manages the logic for displaying city details and navigation to other views.

Classes:
    - VilleController

Functions:
    - __init__(email, ville)
    - update_title()
    - switch_to_accueil(instance, dt='')
    - switch_to_listeReservations_passee(instance)
    - switch_to_listeReservations_future(instance)
    - switch_to_listeVols(instance)
    - switch_to_connexion(instance)
    - switch_to_ville(nomVille)

"""
import requests
from kivy.core.window import Window

from Controller.UserController import UserController
from View.Client.VilleView import VilleView
from Model.UserModel import UserModel


class VilleController:
    """
        Classe: VilleController

        This class manages the logic for displaying city details and navigation to other views.

        Attributes:
            - controller (UserController): Instance of the UserController class for accessing user-related data and functions
            - model (UserModel): Instance of the UserModel class for user-related operations
            - ville (str): The name of the city
            - currentUser (UserModel): The current user
            - view (VilleView): Instance of the VilleView class for displaying city details

        Methods:
            - __init__(email, ville): Constructor method to initialize the VilleController class
            - update_title(): Method to update the title of the city view
            - switch_to_accueil(instance, dt=''): Method to switch to the home screen view
            - switch_to_listeReservations_passee(instance): Method to switch to the past reservations list view
            - switch_to_listeReservations_future(instance): Method to switch to the future reservations list view
            - switch_to_listeVols(instance): Method to switch to the flight list view
            - switch_to_connexion(instance): Method to switch to the login screen view
            - switch_to_ville(nomVille): Method to switch to the city view
        """
    def __init__(self, email, ville):
        """
        Constructor method to initialize the VilleController class.

        Parameters:
            - email (str): Email address of the user
            - ville (str): The name of the city
        """
        self.controller = UserController()
        self.model = UserModel()

        self.ville = ville

        print(self.ville)


        self.currentUser = self.controller.getUtilisateurByEmail(email)

        self.view = VilleView(self)


        Window.set_title("Détails Villes : "+self.ville)

        print(self.currentUser)

        # The openweathermap API key
        self.api_key = '0c7d5506dc9d4709e886c4c2323629b8'

        # Replace 'New York' with the name of the city you want to get weather data for
        self.city =  ville

        weather_data = self.get_weather_data()

        if weather_data:
            print("Weather Data:")
            print(weather_data)
        else:
            print("Failed to retrieve weather data.")

        self.update_title()

    def get_weather_data(self):
        try:
            url = f"http://api.openweathermap.org/data/2.5/weather?q={self.city}&appid={self.api_key}"
            response = requests.get(url)
            response.raise_for_status()  # Raise an exception for 4xx and 5xx status codes
            return response.json()
        except requests.exceptions.RequestException as e:
            print("Error:", e)
            return None

    def update_title(self):
        data = self.get_weather_data()
        if (data != None):
            main_weather = data['weather'][0]['main']
            self.view.titre.text = ("Main weather in " + self.city + " : " + main_weather + " .")
        else:
            self.view.titre.text = ("No found data for this city")

    def switch_to_accueil(self, instance, dt=''):
        """
        Method to switch to the home screen view.

        Parameters:
            - instance: The instance triggering the event (unused)
            - dt (str): Optional parameter representing time (unused)
        """
        # Obligé de faire l'import à l'intérieur d'une méthode afin d'éviter des imports en boucles
        from Controller.Client.AccueilController import AccueilController
        self.accueil_controller = AccueilController(self.currentUser.getEmail())
        self.view.clear_widgets()
        self.view.add_widget(self.accueil_controller.view)



    def switch_to_listeReservations_passee(self, instance):
        """
        Method to switch to the past reservations list view.

        Parameters:
            - instance: The instance triggering the event (unused)
        """
        # Obligé de faire l'import à l'intérieur d'une méthode afin d'éviter des imports en boucles
        from Controller.Client.ListeReservationController import ListeReservationController
        self.listereservation_controller = ListeReservationController(self.currentUser.getEmail(), "passee")
        self.view.clear_widgets()
        self.view.add_widget(self.listereservation_controller.view)



    def switch_to_listeReservations_future(self, instance):
        """
        Method to switch to the future reservations list view.

        Parameters:
            - instance: The instance triggering the event (unused)
        """
        # Obligé de faire l'import à l'intérieur d'une méthode afin d'éviter des imports en boucles
        from Controller.Client.ListeReservationController import ListeReservationController
        self.listereservation_controller = ListeReservationController(self.currentUser.getEmail(), "future")
        self.view.clear_widgets()
        self.view.add_widget(self.listereservation_controller.view)



    def switch_to_listeVols(self, instance):
        """
        Method to switch to the flight list view.

        Parameters:
            - instance: The instance triggering the event (unused)
        """
        # Obligé de faire l'import à l'intérieur d'une méthode afin d'éviter des imports en boucles
        from Controller.Client.ListeVolsController import ListeVolsController
        self.listevols_controller = ListeVolsController(self.currentUser.getEmail())
        self.view.clear_widgets()
        self.view.add_widget(self.listevols_controller.view)



    def switch_to_connexion(self, instance):
        """
        Method to switch to the city view.

        Parameters:
            - nomVille: The name of the city
        """
        """
        Method to switch to the login screen view.

        Parameters:
            - instance: The instance triggering the event (unused)
        """
        # Obligé de faire l'import à l'intérieur d'une méthode afin d'éviter des imports en boucles
        from Controller.ConnexionController import ConnexionController
        self.currentUser = ""
        self.connexion_controller = ConnexionController()
        self.view.clear_widgets()
        self.view.add_widget(self.connexion_controller.view)


    def switch_to_ville(self, nomVille):
        # Obligé de faire l'import à l'intérieur d'une méthode afin d'éviter des imports en boucles
        from Controller.Client.VilleController import VilleController
        self.currentUser = ""
        self.ville_controller = VilleController()
        self.view.clear_widgets()
        self.view.add_widget(self.ville_controller.view)