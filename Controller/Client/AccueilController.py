import requests

from kivy.core.window import Window

from Controller.UserController import UserController
from View.Client.AccueilView import AccueilView
from Model.UserModel import UserModel



class AccueilController:

    def __init__(self, email):
        self.controller = UserController()
        self.model = UserModel()
        self.view = AccueilView(self)
        self.currentUser = self.controller.getUtilisateurByEmail(email)
        Window.set_title("Accueil")

        #The openweathermap API key
        self.api_key = '0c7d5506dc9d4709e886c4c2323629b8'

        # Replace 'New York' with the name of the city you want to get weather data for
        self.city = 'France'

        weather_data = self.get_weather_data()

        if weather_data:
            print("Weather Data:")
            print(weather_data)
        else:
            print("Failed to retrieve weather data.")



        self.update_title()



    def get_weather_data(self):
        try:
            url = f"http://api.openweathermap.org/data/2.5/weather?q={self.city}&appid={self.api_key}"
            response = requests.get(url)
            response.raise_for_status()  # Raise an exception for 4xx and 5xx status codes
            return response.json()
        except requests.exceptions.RequestException as e:
            print("Error:", e)
            return None



    def update_title(self, data=''):
        self.view.titre.text = "Bienvenue " + self.currentUser.getNom() + " " + self.currentUser.getPrenom() + " !"

        data = self.get_weather_data()
        if (data != None):
            main_weather = data['weather'][0]['main']
            self.view.titre.text = ("Bienvenue " + self.currentUser.getNom() + " " + self.currentUser.getPrenom() + " ! \n Main weather in " + self.city + " : " + main_weather + " .")



    def switch_to_accueil(self, instance, dt=''):
        #Obligé de faire l'import à l'intérieur d'une méthode afin d'éviter des imports en boucles
        from Controller.Client.AccueilController import AccueilController
        self.accueil_controller = AccueilController(self.currentUser.getEmail())
        self.view.clear_widgets()
        self.view.add_widget(self.accueil_controller.view)


    def switch_to_listeReservations_passee(self, instance):
        #Obligé de faire l'import à l'intérieur d'une méthode afin d'éviter des imports en boucles
        from Controller.Client.ListeReservationController import ListeReservationController
        self.listereservation_controller = ListeReservationController(self.currentUser.getEmail(),"passee")
        self.view.clear_widgets()
        self.view.add_widget(self.listereservation_controller.view)



    def switch_to_listeReservations_future(self, instance):
        #Obligé de faire l'import à l'intérieur d'une méthode afin d'éviter des imports en boucles
        from Controller.Client.ListeReservationController import ListeReservationController
        self.listereservation_controller = ListeReservationController(self.currentUser.getEmail(), "future")
        self.view.clear_widgets()
        self.view.add_widget(self.listereservation_controller.view)



    def switch_to_listeVols(self, instance):
        #Obligé de faire l'import à l'intérieur d'une méthode afin d'éviter des imports en boucles
        from Controller.Client.ListeVolsController import ListeVolsController
        self.listevols_controller = ListeVolsController(self.currentUser.getEmail())
        self.view.clear_widgets()
        self.view.add_widget(self.listevols_controller.view)


    def switch_to_connexion(self, instance):
        #Obligé de faire l'import à l'intérieur d'une méthode afin d'éviter des imports en boucles
        from Controller.ConnexionController import ConnexionController
        self.currentUser = ""
        self.connexion_controller = ConnexionController()
        self.view.clear_widgets()
        self.view.add_widget(self.connexion_controller.view)


