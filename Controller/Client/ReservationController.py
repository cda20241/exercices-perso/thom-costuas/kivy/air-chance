"""
Module: ReservationController

This module contains the ReservationController class, which manages the logic for making flight reservations for a specific user.

Classes:
    - ReservationController

Functions:
    - __init__(email, numVol)
    - update_title()
    - estDejaReserve()
    - formatDonneesReservation()
    - reserver(instance)
    - switch_to_accueil(instance, dt='')
    - switch_to_listeReservations_passee(instance)
    - switch_to_listeReservations_future(instance)
    - switch_to_listeVols(instance)
    - switch_to_connexion(instance)
    - switch_to_ville(instance)

"""


import re
from kivy.core.window import Window
from datetime import date

from Controller.BilletController import BilletController
from Controller.UserController import UserController
from Controller.VolController import VolController
from Model.BilletModel import BilletModel
from Model.UserModel import UserModel
from View.Client.ReservationView import ReservationView


class ReservationController:
    """
       Classe: ReservationController

       This class manages the logic for making flight reservations for a specific user.

       Attributes:
           - controller (UserController): Instance of the UserController class for accessing user-related data and functions
           - controller_billet (BilletController): Instance of the BilletController class for accessing ticket-related data and functions
           - controller_vol (VolController): Instance of the VolController class for accessing flight-related data and functions
           - model (UserModel): Instance of the UserModel class for user-related operations
           - model_billet (BilletModel): Instance of the BilletModel class for ticket-related operations
           - currentUser (UserModel): The current user for whom flight reservations are being made
           - vol (VolModel): The flight for which reservation is being made
           - lstData (list): List containing formatted flight data for display
           - lstBillets (list): List containing ticket data for the current user
           - view (ReservationView): Instance of the ReservationView class for displaying reservation-related data

       Methods:
           - __init__(email, numVol): Constructor method to initialize the ReservationController class
           - update_title(): Method to update the title of the reservation view
           - estDejaReserve(): Method to check if the user has already made a reservation for the flight
           - formatDonneesReservation(): Method to format flight data for display
           - reserver(instance): Method to make a flight reservation
           - switch_to_accueil(instance, dt=''): Method to switch to the home screen view
           - switch_to_listeReservations_passee(instance): Method to switch to the past reservations list view
           - switch_to_listeReservations_future(instance): Method to switch to the future reservations list view
           - switch_to_listeVols(instance): Method to switch to the flight list view
           - switch_to_connexion(instance): Method to switch to the login screen view
           - switch_to_ville(instance): Method to switch to the city view
       """
    def __init__(self, email, numVol):
        """
        Constructor method to initialize the ReservationController class.

        Parameters:
            - email (str): Email address of the user
            - numVol (str): Flight number for which reservation is being made
        """
        self.controller = UserController()
        self.controller_billet = BilletController()
        self.controller_vol = VolController()

        self.model = UserModel()
        self.model_billet = BilletModel()

        self.currentUser = self.controller.getUtilisateurByEmail(email)
        self.vol = self.controller_vol.getVolByNumVol(numVol)

        self.lstData = self.formatDonneesReservation()
        self.lstBillets = self.controller_billet.lstBilletsByEmail(self.currentUser.getEmail())

        self.view = ReservationView(self, self.lstData, self.estDejaReserve())

        Window.set_title("Réservation vol ")
        print(self.currentUser)
        self.update_title()

    def update_title(self):
        """
        Method to update the title of the reservation view.
        """
        self.view.titre.text = "Espace reservation vol"



    def estDejaReserve(self):
        """
        Method to check if the user has already made a reservation for the flight.

        Returns:
            bool: True if the user has already made a reservation, False otherwise
        """
        for unBillet in self.lstBillets:
            if unBillet.getVol().getNumVol() == self.vol.getNumVol():
                return True
        return False


    def formatDonneesReservation(self):
        """
        Method to format flight data for display.

        Returns:
            list: List containing formatted flight data
        """
        data = []
        leVol = self.vol
        ligne = {
            "NumVol": leVol.getNumVol(),
            "DateDepart": leVol.getDateDepart(),
            "DateArrivee": leVol.getDateArrivee(),
            "HeureDepart": leVol.getHeureDepart(),
            "HeureArrivee": leVol.getHeureArrivee(),
            "VilleDepart": leVol.getVilleDepart(),
            "VilleArrivee": leVol.getVilleArrivee(),
            "Retard": leVol.getRetard(),
            "Avion": leVol.getAvion()
        }
        data.append(ligne)
        return data

    def reserver(self, instance):
        """
        Method to make a flight reservation.

        Parameters:
            - instance: The instance triggering the event
        """
        dateActuelle = date.today()

        billet = BilletModel(client=self.currentUser.getEmail(),
                             vol=self.vol.getNumVol(),
                             numBillet=str(int(self.controller_billet.getLastBillet().getNumBillet())+1),
                             dateEmission=dateActuelle,
                             dateReservation= dateActuelle,
                             datePaiement=dateActuelle)
        self.controller_billet.ajouterBillet(billet)
        self.switch_to_accueil(instance)



    def switch_to_accueil(self, instance, dt=''):
        """
        Method to switch to the home screen view.

        Parameters:
            - instance: The instance triggering the event (unused)
            - dt (str): Optional parameter representing time (unused)
        """
        # Obligé de faire l'import à l'intérieur d'une méthode afin d'éviter des imports en boucles
        from Controller.Client.AccueilController import AccueilController
        self.accueil_controller = AccueilController(self.currentUser.getEmail())
        self.view.clear_widgets()
        self.view.add_widget(self.accueil_controller.view)



    def switch_to_listeReservations_passee(self, instance):
        """
        Method to switch to the past reservations list view.

        Parameters:
            - instance: The instance triggering the event (unused)
        """
        # Obligé de faire l'import à l'intérieur d'une méthode afin d'éviter des imports en boucles
        from Controller.Client.ListeReservationController import ListeReservationController
        self.listereservation_controller = ListeReservationController(self.currentUser.getEmail(), "passee")
        self.view.clear_widgets()
        self.view.add_widget(self.listereservation_controller.view)



    def switch_to_listeReservations_future(self, instance):
        """
        Method to switch to the future reservations list view.

        Parameters:
            - instance: The instance triggering the event (unused)
        """
        # Obligé de faire l'import à l'intérieur d'une méthode afin d'éviter des imports en boucles
        from Controller.Client.ListeReservationController import ListeReservationController
        self.listereservation_controller = ListeReservationController(self.currentUser.getEmail(), "future")
        self.view.clear_widgets()
        self.view.add_widget(self.listereservation_controller.view)



    def switch_to_listeVols(self, instance):
        """
        Method to switch to the flight list view.

        Parameters:
            - instance: The instance triggering the event (unused)
        """
        # Obligé de faire l'import à l'intérieur d'une méthode afin d'éviter des imports en boucles
        from Controller.Client.ListeVolsController import ListeVolsController
        self.listevols_controller = ListeVolsController(self.currentUser.getEmail())
        self.view.clear_widgets()
        self.view.add_widget(self.listevols_controller.view)



    def switch_to_connexion(self, instance):
        """
        Method to switch to the login screen view.

        Parameters:
            - instance: The instance triggering the event (unused)
        """
        # Obligé de faire l'import à l'intérieur d'une méthode afin d'éviter des imports en boucles
        from Controller.ConnexionController import ConnexionController
        self.currentUser = ""
        self.connexion_controller = ConnexionController()
        self.view.clear_widgets()
        self.view.add_widget(self.connexion_controller.view)


    def switch_to_ville(self, instance):
        """
        Method to switch to the city view.

        Parameters:
            - instance: The instance triggering the event
        """
        # Obligé de faire l'import à l'intérieur d'une méthode afin d'éviter des imports en boucles
        from Controller.Client.VilleController import VilleController

        self.ville = re.sub(r'\[.*?\]', '', instance.text)
        self.ville_controller = VilleController(self.currentUser.getEmail(), self.ville)
        self.view.clear_widgets()
        self.view.add_widget(self.ville_controller.view)