"""
Module: InscriptionController

This module contains the InscriptionController class, which manages user registration logic and navigation.

Classes:
    - InscriptionController

Functions:
    - __init__()
    - valider_inscription(instance)
    - switch_to_connexion(dt)
    - update_label(success)
"""

from kivy.clock import Clock
from kivy.core.window import Window

from Controller.UserController import UserController
from View.InscriptionView import InscriptionView
from Model.UserModel import UserModel


class InscriptionController:
    """
    Classe: InscriptionController

    This class manages user registration logic and navigation.

    Attributes:
        - controller (UserController): Instance of UserController for managing user data
        - model (UserModel): Instance of UserModel for user data modeling
        - view (InscriptionView): Instance of InscriptionView for user interface
    """
    def __init__(self):
        """
        Constructor method to initialize the InscriptionController class.
        """
        self.controller = UserController()
        self.model = UserModel()
        self.view = InscriptionView(self)
        Window.set_title("Inscription")

    def valider_inscription(self, instance):
        """
        Method to validate user registration.

        Parameters:
            - instance: The instance triggering the event
        """
        prenom = self.view.input_prenom.text
        nom = self.view.input_nom.text
        email = self.view.input_email.text
        mdp = self.view.input_mdp.text
        statut = self.view.btn_dropdown_menu.text

        if prenom != "" and nom!= "" and email != "" and mdp != "" and statut != "Choisissez une option":
            if self.controller.getUtilisateurByEmail(email) == -1:
                self.update_label(True)

                self.controller.ajouterUtilisateur(UserModel(prenom, nom, email, mdp, statut))
                tempsAttenteConnexion = 2
                Clock.schedule_once(self.switch_to_connexion, tempsAttenteConnexion)
            else:
                self.update_label(None)
        else:
            self.update_label(False)


    # In InscriptionController
    def switch_to_connexion(self, dt):
        """
        Method to switch to the login view.

        Parameters:
            - dt: The delay time before switching views
        """
        from Controller.ConnexionController import ConnexionController
        self.connexion_controller = ConnexionController()
        self.view.clear_widgets()
        self.view.add_widget(self.connexion_controller.view)



    def update_label(self, success):
        """
        Method to update the label text based on registration success or failure.

        Parameters:
            - success (bool): Boolean indicating if registration was successful
        """
        if success== True:
            self.view.label.text = ("Inscription réussie ! Redirection...")
        elif success == False:
            self.view.label.text = "Tous les champs n'ont pas été renseignés !"
        elif success == None:
            self.view.label.text = "Cette adresse email est déjà utilisée !"

