"""
Module: UserController

This module contains the UserController class, which manages user data, including loading from and saving to CSV files.

Classes:
    - UserController

Functions:
    - __init__()
    - loadUtilisateurs()
    - consoleAfficheUtilisateurs()
    - getLstUtilisateurs()
    - getUtilisateurByEmail(email)
    - identifiantsValide(email, mdp)
    - ajouterUtilisateur(unUtilisateur)
"""

import csv
from pathlib import Path

from Controller.BilletController import BilletController
from Model.UserModel import UserModel

class UserController:
    """
    Classe: UserController

    This class manages user data, including loading from and saving to CSV files.

    Attributes:
        - filePath (Path): Path to the CSV file containing user data
        - controller_billet (BilletController): Instance of BilletController for managing ticket data
        - colonnePrenom (int): Index of the column containing user first names in the CSV file
        - colonneNom (int): Index of the column containing user last names in the CSV file
        - colonneEmail (int): Index of the column containing user emails in the CSV file
        - colonneMdp (int): Index of the column containing user passwords in the CSV file
        - colonneStatut (int): Index of the column containing user statuses in the CSV file
        - colonneBillet (int): Index of the column containing user tickets in the CSV file
        - lstUtilisateur (list): List containing UserModel objects representing users
    """
    def __init__(self):
        """
        Constructor method to initialize the UserController class.
        """
        self.filePath = Path(__file__).parent / "../Assests/Utilisateurs.csv"

        self.controller_billet = BilletController()

        self.colonnePrenom = 0
        self.colonneNom = 1
        self.colonneEmail = 2
        self.colonneMdp = 3
        self.colonneStatut = 4
        self.colonneBillet = 5

        self.lstUtilisateur = self.loadUtilisateurs()


    def loadUtilisateurs(self):
        """
        Method to load user data from a CSV file.

        Returns:
            list: List of UserModel objects representing users
        """
        # Remplace le chemin par le chemin réel de ton fichier CSV
        chemin_fichier_csv = self.filePath

        result = []

        # Lire le fichier CSV
        with open(chemin_fichier_csv, 'r') as fichier_csv:
            lecteur_csv = csv.reader(fichier_csv)

            # Ignorer l'en-tête si le fichier CSV en a un
            en_tete = next(lecteur_csv, None)

            # Parcourir les lignes du fichier CSV
            for ligne in lecteur_csv:
                # Créer un objet UserModel pour chaque ligne

                lstBillets = self.controller_billet.lstBilletsByEmail(email=ligne[self.colonneEmail])

                utilisateur = UserModel(prenom=ligne[self.colonnePrenom],
                                        nom=ligne[self.colonneNom],
                                        email=ligne[self.colonneEmail],
                                        mdp=ligne[self.colonneMdp],
                                        statut=ligne[self.colonneStatut],
                                        lstBillets = lstBillets)

                # Ajouter l'utilisateur à la liste principale
                result.append(utilisateur)
        return result

    def consoleAfficheUtilisateurs(self):
        """
        Method to display user data in the console.
        """
        # Afficher les objets UserModel dans la liste
        for utilisateur in self.lstUtilisateur:
            print(utilisateur, "\n")

    def getLstUtilisateurs(self):
        """
        Method to retrieve the list of users.

        Returns:
            list: List of UserModel objects representing users
        """
        return self.lstUtilisateur


    def getUtilisateurByEmail(self, email):
        """
        Method to retrieve a user by email.

        Parameters:
            email (str): Email of the user to retrieve

        Returns:
            UserModel or int: UserModel object representing the user, or -1 if not found
        """
        for unUtilisateur in self.lstUtilisateur:
            if unUtilisateur.getEmail() == email:
                return unUtilisateur
        return -1  #Indique que l'on n'a pas trouvé l'utilisateur dans la BDD


    def identifiantsValide(self, email, mdp):
        """
        Method to validate user credentials.

        Parameters:
            email (str): Email of the user
            mdp (str): Password of the user

        Returns:
            bool: True if credentials are valid, False otherwise
        """
        print("\n")
        for unUtilisateur in self.lstUtilisateur:
            if unUtilisateur.getEmail().__eq__(email) and unUtilisateur.getMdp().__eq__(mdp):
                return True
        return False

    def ajouterUtilisateur(self, unUtilisateur):
        """
        Method to add a new user.

        Parameters:
            unUtilisateur (UserModel): UserModel object representing the new user
        """
        # Add the new user to the list in memory
        self.lstUtilisateur.append(unUtilisateur)

        # Write the updated list to the CSV file
        with open(self.filePath, 'a', newline='') as fichier_csv:
            writer = csv.writer(fichier_csv)
            writer.writerow([
                unUtilisateur.getPrenom(),
                unUtilisateur.getNom(),
                unUtilisateur.getEmail(),
                unUtilisateur.getMdp(),
                unUtilisateur.getStatut()
            ])