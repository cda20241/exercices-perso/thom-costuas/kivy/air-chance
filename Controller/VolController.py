"""
Module: VolController

This module contains the VolController class, which manages flight data, including loading from and saving to CSV files.

Classes:
    - VolController

Functions:
    - __init__()
    - loadVols()
    - getVolByNumVol(numVol)
    - ajouterVol(unVol)
    - modifierVol(oldVol, newVol)
    - annulerVol(numVol)
    - getLstAllVols()
"""

import csv
from pathlib import Path

from Controller.AvionController import AvionController
from Model.VolModel import VolModel


class VolController:
    """
    Classe: VolController

    This class manages flight data, including loading from and saving to CSV files.

    Attributes:
        - filePath (Path): Path to the CSV file containing flight data
        - controller_avion (AvionController): Instance of AvionController for managing aircraft data
        - colonneNumVol (int): Index of the column containing flight numbers in the CSV file
        - colonneDateDepart (int): Index of the column containing departure dates in the CSV file
        - colonneDateArrivee (int): Index of the column containing arrival dates in the CSV file
        - colonneHeureDepart (int): Index of the column containing departure times in the CSV file
        - colonneHeureArrivee (int): Index of the column containing arrival times in the CSV file
        - colonneVilleDepart (int): Index of the column containing departure cities in the CSV file
        - colonneVilleArrivee (int): Index of the column containing arrival cities in the CSV file
        - colonneRetard (int): Index of the column containing flight delays in the CSV file
        - colonneAvion (int): Index of the column containing aircraft information in the CSV file
        - lstAllVols (list): List containing VolModel objects representing flights
    """
    def __init__(self):
        """
        Constructor method to initialize the VolController class.
        """
        self.filePath = Path(__file__).parent / "../Assests/Vols.csv"

        self.controller_avion = AvionController()

        self.colonneNumVol = 0
        self.colonneDateDepart = 1
        self.colonneDateArrivee = 2
        self.colonneHeureDepart = 3
        self.colonneHeureArrivee = 4
        self.colonneVilleDepart = 5
        self.colonneVilleArrivee = 6
        self.colonneRetard = 7
        self.colonneAvion = 8

        self.lstAllVols = self.loadVols()

    def loadVols(self):
        """
        Method to load flight data from a CSV file.

        Returns:
            list: List of VolModel objects representing flights
        """
        # Remplace le chemin par le chemin réel de ton fichier CSV
        chemin_fichier_csv = self.filePath

        result = []

        # Lire le fichier CSV
        with open(chemin_fichier_csv, 'r') as fichier_csv:
            lecteur_csv = csv.reader(fichier_csv)

            # Ignorer l'en-tête si le fichier CSV en a un
            en_tete = next(lecteur_csv, None)

            # Parcourir les lignes du fichier CSV
            for ligne in lecteur_csv:
                # Créer un objet UserModel pour chaque ligne

                unAvion = ligne[self.colonneAvion]

                vol = VolModel(numvol=ligne[self.colonneNumVol],
                               dateDepart=ligne[self.colonneDateDepart],
                               dateArrivee=ligne[self.colonneDateArrivee],
                               heureDepart=ligne[self.colonneHeureDepart],
                               heureArrivee=ligne[self.colonneHeureArrivee],
                               villeDepart=ligne[self.colonneVilleDepart],
                               villeArrivee=ligne[self.colonneVilleArrivee],
                               retard=ligne[self.colonneRetard],
                               avion=unAvion)

                # Ajouter le billet à la liste principale
                result.append(vol)
        return result

    def getVolByNumVol(self, numVol):
        """
            Method to retrieve a flight by its number.

            Parameters:
                numVol (str): Number of the flight to retrieve

            Returns:
                VolModel or int: VolModel object representing the flight, or -1 if not found
        """
        for unVol in self.lstAllVols:
            if unVol.getNumVol() == numVol:
                return unVol
        return -1  #Indique que l'on n'a pas trouvé l'utilisateur dans la BDD

    def ajouterVol(self, unVol):
        """
        Method to add a new flight.

        Parameters:
            unVol (VolModel): VolModel object representing the new flight
        """
        self.lstAllVols.append(unVol)

        # Write the updated list to the CSV file
        with open(self.filePath, 'a', newline='') as fichier_csv:
            writer = csv.writer(fichier_csv)
            writer.writerow([
                unVol.getNumVol(),
                unVol.getDateDepart(),
                unVol.getDateArrivee(),
                unVol.getHeureDepart(),
                unVol.getHeureArrivee(),
                unVol.getVilleDepart(),
                unVol.getVilleArrivee(),
                unVol.getRetard(),
                unVol.getAvion(),
            ])


    def modifierVol(self, oldVol, newVol):
        """
        Method to modify an existing flight.

        Parameters:
            oldVol (dict): Dictionary containing data of the old flight
            newVol (VolModel): VolModel object representing the new flight
        """
        # Charger toutes les lignes existantes du fichier CSV
        with open(self.filePath, 'r', newline='') as csv_file:
            csv_reader = csv.reader(csv_file)
            rows = list(csv_reader)

        # Trouver la ligne à modifier en utilisant le numéro de vol
        for index, row in enumerate(rows):
            if row[self.colonneNumVol] == oldVol["NumVol"]:
                # Modifier les données de cette ligne
                rows[index] = [
                    newVol.getNumVol(),
                    newVol.getDateDepart(),
                    newVol.getDateArrivee(),
                    newVol.getHeureDepart(),
                    newVol.getHeureArrivee(),
                    newVol.getVilleDepart(),
                    newVol.getVilleArrivee(),
                    newVol.getRetard(),
                    newVol.getAvion(),
                ]
                break

        # Réécrire toutes les lignes dans le fichier CSV
        with open(self.filePath, 'w', newline='') as csv_file:
            csv_writer = csv.writer(csv_file)
            csv_writer.writerows(rows)

        from Controller.BilletController import BilletController
        self.controller_billet = BilletController()
        self.controller_billet.modifierBillet(oldVol["NumVol"], newVol.getNumVol())

    def annulerVol(self, numVol):
        """
        Method to cancel a flight.

        Parameters:
            numVol (str): Number of the flight to cancel
        """
        # Charger toutes les lignes existantes du fichier CSV
        with open(self.filePath, 'r', newline='') as csv_file:
            csv_reader = csv.reader(csv_file)
            rows = list(csv_reader)

        # Créer une nouvelle liste de lignes sans la ligne à supprimer
        updated_rows = [row for row in rows if row[self.colonneNumVol] != numVol]

        # Réécrire toutes les lignes sauf celle à supprimer dans le fichier CSV
        with open(self.filePath, 'w', newline='') as csv_file:
            csv_writer = csv.writer(csv_file)
            csv_writer.writerows(updated_rows)

        self.lstAllVols = self.loadVols()

        # Appeler la méthode d'annulation de billet avec le numéro de vol
        from Controller.BilletController import BilletController
        self.controller_billet = BilletController()
        self.controller_billet.annulerBillet(numVol)



    def getLstAllVols(self):
        """
        Method to retrieve the list of all flights.

        Returns:
            list: List of VolModel objects representing all flights
        """
        return self.lstAllVols