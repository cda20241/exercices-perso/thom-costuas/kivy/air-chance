"""
Class: UserModel

This class represents a model for a user, storing information such as first name, last name, email, password, status, and a list of tickets associated with the user.

Attributes:
    - prenom (str): First name of the user
    - nom (str): Last name of the user
    - email (str): Email address of the user
    - mdp (str): Password of the user
    - statut (str): Status of the user (e.g., Client, Controleur)
    - lstBillets (list): List of tickets associated with the user

Methods:
    - __init__(self, prenom='', nom='', email='', mdp='', statut='', lstBillets=[]): Constructor method to initialize a UserModel object
    - getPrenom(self): Method to retrieve the first name of the user
    - getNom(self): Method to retrieve the last name of the user
    - getEmail(self): Method to retrieve the email address of the user
    - getMdp(self): Method to retrieve the password of the user
    - getStatut(self): Method to retrieve the status of the user
    - getLstBillets(self): Method to retrieve the list of tickets associated with the user
    - setLstBillets(self, lstBillets): Method to set the list of tickets associated with the user
    - __str__(self): Method to return a string representation of the UserModel object
"""


class UserModel:

    def __init__(self, prenom='', nom='', email='', mdp='', statut='', lstBillets=[]):
        """
        Constructor method to initialize a UserModel object.

        Parameters:
            prenom (str): First name of the user (default='')
            nom (str): Last name of the user (default='')
            email (str): Email address of the user (default='')
            mdp (str): Password of the user (default='')
            statut (str): Status of the user (e.g., Client, Controleur) (default='')
            lstBillets (list): List of tickets associated with the user (default=[])
        """
        self.prenom = prenom
        self.nom = nom
        self.email = email
        self.mdp = mdp
        self.statut = statut
        self.lstBillets = lstBillets


    def getPrenom(self):
        """
        Method to retrieve the first name of the user.

        Returns:
            str: First name of the user
        """
        return self.prenom

    def getNom(self):
        """
        Method to retrieve the last name of the user.

        Returns:
            str: Last name of the user
        """
        return self.nom

    def getEmail(self):
        """
        Method to retrieve the email address of the user.

        Returns:
            str: Email address of the user
        """
        return self.email

    def getMdp(self):
        """
        Method to retrieve the password of the user.

        Returns:
            str: Password of the user
        """
        return self.mdp

    def getStatut(self):
        """
        Method to retrieve the status of the user.

        Returns:
            str: Status of the user
        """
        return self.statut

    def getLstBillets(self):
        """
        Method to retrieve the list of tickets associated with the user.

        Returns:
            list: List of tickets associated with the user
        """
        return self.lstBillets

    def setLstBillets(self, lstBillets):
        """
        Method to set the list of tickets associated with the user.

        Parameters:
            lstBillets (list): List of tickets associated with the user
        """
        self.lstBillets = lstBillets

    def __str__(self):
        """
        Method to return a string representation of the UserModel object.

        Returns:
            str: String representation of the UserModel object
        """
        billets_str = ', '.join(map(str, self.lstBillets))

        return (f"Prenom: {self.prenom}, "
                f"Nom: {self.nom}, "
                f"Email: {self.email}, "
                f"MDP: {self.mdp}, "
                f"Statut: {self.statut}, "
                f"Liste Billets: [{billets_str}]")