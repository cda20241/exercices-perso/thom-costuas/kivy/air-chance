"""
Class: VolModel

This class represents a model for a flight, storing information such as flight number, departure date, arrival date, departure time, arrival time, departure city, arrival city, delay status, and aircraft.

Attributes:
    - numvol (str): Flight number
    - dateDepart (str): Departure date of the flight
    - dateArrivee (str): Arrival date of the flight
    - heureDepart (str): Departure time of the flight
    - heureArrivee (str): Arrival time of the flight
    - villeDepart (str): Departure city of the flight
    - villeArrivee (str): Arrival city of the flight
    - retard (str): Delay status of the flight
    - avion (str): Aircraft associated with the flight

Methods:
    - __init__(self, numvol='', dateDepart='', dateArrivee='', heureDepart='', heureArrivee='', villeDepart='', villeArrivee='', retard='', avion=''): Constructor method to initialize a VolModel object
    - getNumVol(self): Method to retrieve the flight number
    - getDateDepart(self): Method to retrieve the departure date of the flight
    - getDateArrivee(self): Method to retrieve the arrival date of the flight
    - getHeureDepart(self): Method to retrieve the departure time of the flight
    - getHeureArrivee(self): Method to retrieve the arrival time of the flight
    - getVilleDepart(self): Method to retrieve the departure city of the flight
    - getVilleArrivee(self): Method to retrieve the arrival city of the flight
    - getRetard(self): Method to retrieve the delay status of the flight
    - getAvion(self): Method to retrieve the aircraft associated with the flight
    - __str__(self): Method to return a string representation of the VolModel object
"""

class VolModel:
    def __init__(self, numvol="", dateDepart="", dateArrivee="", heureDepart="", heureArrivee="", villeDepart="", villeArrivee="", retard="", avion=""):
        """
        Constructor method to initialize a VolModel object.

        Parameters:
            numvol (str): Flight number (default='')
            dateDepart (str): Departure date of the flight (default='')
            dateArrivee (str): Arrival date of the flight (default='')
            heureDepart (str): Departure time of the flight (default='')
            heureArrivee (str): Arrival time of the flight (default='')
            villeDepart (str): Departure city of the flight (default='')
            villeArrivee (str): Arrival city of the flight (default='')
            retard (str): Delay status of the flight (default='')
            avion (str): Aircraft associated with the flight (default='')
        """

        self.numvol = numvol
        self.dateDepart= dateDepart
        self.dateArrivee = dateArrivee
        self.heureDepart = heureDepart
        self.heureArrivee = heureArrivee
        self.villeDepart = villeDepart
        self.villeArrivee = villeArrivee
        self.retard = retard
        self.avion = avion

    def getNumVol(self):
        """
        Method to retrieve the flight number.

        Returns:
            str: Flight number
        """
        return self.numvol

    def getDateDepart(self):
        """
        Method to retrieve the departure date of the flight.

        Returns:
            str: Departure date of the flight
        """
        return self.dateDepart

    def getDateArrivee(self):
        """
        Method to retrieve the arrival date of the flight.

        Returns:
            str: Arrival date of the flight
        """
        return self.dateArrivee

    def getHeureDepart(self):
        """
        Method to retrieve the departure time of the flight.

        Returns:
            str: Departure time of the flight
        """
        return self.heureDepart

    def getHeureArrivee(self):
        """
        Method to retrieve the arrival time of the flight.

        Returns:
            str: Arrival time of the flight
        """
        return self.heureArrivee

    def getVilleDepart(self):
        """
        Method to retrieve the departure city of the flight.

        Returns:
            str: Departure city of the flight
        """
        return self.villeDepart

    def getVilleArrivee(self):
        """
        Method to retrieve the arrival city of the flight.

        Returns:
            str: Arrival city of the flight
        """
        return self.villeArrivee

    def getRetard(self):
        """
        Method to retrieve the delay status of the flight.

        Returns:
            str: Delay status of the flight
        """
        return self.retard

    def getAvion(self):
        """
        Method to retrieve the aircraft associated with the flight.

        Returns:
            str: Aircraft associated with the flight
        """
        return self.avion

    def __str__(self):
        """
        Method to return a string representation of the VolModel object.

        Returns:
            str: String representation of the VolModel object
        """
        return (f"Vol -> ["
                f"NumVol: {self.numvol}, "
                f"DateDepart: {self.dateDepart}, "
                f"DateArrivee: {self.dateArrivee}, "
                f"HeureDepart: {self.heureDepart}, "
                f"HeureArrivee: {self.heureArrivee}, "
                f"VilleDepart: {self.villeDepart}, "
                f"VilleArrivee: {self.villeArrivee}, "
                f"Retard: {self.retard}, "
                f"Avion: {self.avion} ]")