"""
Class: AvionModel

This class represents a model for an aircraft, storing its attributes such as code, type, model, and passenger capacity.

Attributes:
    - codeAvion (str): Code of the aircraft
    - typeAvion (str): Type of the aircraft
    - modele (str): Model of the aircraft
    - nbPassagers (int): Number of passengers the aircraft can accommodate

Methods:
    - __init__(self, codeAvion="", typeAvion="", modele="", nbPassagers=0): Constructor method to initialize an AvionModel object
    - getCodeAvion(self): Method to retrieve the code of the aircraft
    - getTypeAvion(self): Method to retrieve the type of the aircraft
    - getModele(self): Method to retrieve the model of the aircraft
    - getNbPassagers(self): Method to retrieve the number of passengers the aircraft can accommodate
    - __str__(self): Method to return a string representation of the AvionModel object
"""

class AvionModel:
    def __init__(self, codeAvion="", typeAvion="", modele="", nbPassagers=0):
        """
        Constructor method to initialize an AvionModel object.

        Parameters:
            codeAvion (str): Code of the aircraft (default="")
            typeAvion (str): Type of the aircraft (default="")
            modele (str): Model of the aircraft (default="")
            nbPassagers (int): Number of passengers the aircraft can accommodate (default=0)
        """
        self.codeAvion = codeAvion
        self.typeAvion= typeAvion
        self.modele = modele
        self.nbPassagers = nbPassagers

    def getCodeAvion(self):
        """
        Method to retrieve the code of the aircraft.

        Returns:
            str: Code of the aircraft
        """
        return self.codeAvion

    def getTypeAvion(self):
        """
        Method to retrieve the type of the aircraft.

        Returns:
            str: Type of the aircraft
        """
        return self.typeAvion

    def getModele(self):
        """
        Method to retrieve the model of the aircraft.

        Returns:
            str: Model of the aircraft
        """
        return self.modele

    def getNbPassagers(self):
        """
        Method to retrieve the number of passengers the aircraft can accommodate.

        Returns:
            int: Number of passengers the aircraft can accommodate
        """
        return self.nbPassagers

    def __str__(self):
        """
        Method to return a string representation of the AvionModel object.

        Returns:
            str: String representation of the AvionModel object
        """
        return (f"Code Avion: {self.codeAvion}, "
                f"Type Avion: {self.typeAvion}, "
                f"Modele: {self.modele}, "
                f"Nombre de Passagers: {self.nbPassagers}")
