"""
Class: BilletModel

This class represents a model for a flight ticket, storing information such as client, flight, ticket number, dates of emission, reservation, and payment.

Attributes:
    - client (str): Client associated with the ticket
    - vol (str): Flight associated with the ticket
    - numBillet (int): Number of the ticket
    - dateEmission (str): Date of ticket emission
    - dateReservation (str): Date of ticket reservation
    - datePaiement (str): Date of ticket payment

Methods:
    - __init__(self, client="", vol="", numBillet=0, dateEmission="", dateReservation="", datePaiement=""): Constructor method to initialize a BilletModel object
    - getClient(self): Method to retrieve the client associated with the ticket
    - getVol(self): Method to retrieve the flight associated with the ticket
    - getNumBillet(self): Method to retrieve the number of the ticket
    - getDateEmission(self): Method to retrieve the date of ticket emission
    - getDateReservation(self): Method to retrieve the date of ticket reservation
    - getDatePaiement(self): Method to retrieve the date of ticket payment
    - __str__(self): Method to return a string representation of the BilletModel object
"""

class BilletModel:

    def __init__(self, client="", vol="", numBillet=0, dateEmission="", dateReservation="", datePaiement=""):
        """
        Constructor method to initialize a BilletModel object.

        Parameters:
            client (str): Client associated with the ticket (default="")
            vol (str): Flight associated with the ticket (default="")
            numBillet (int): Number of the ticket (default=0)
            dateEmission (str): Date of ticket emission (default="")
            dateReservation (str): Date of ticket reservation (default="")
            datePaiement (str): Date of ticket payment (default="")
        """
        self.client = client
        self.vol = vol
        self.numBillet = numBillet
        self.dateEmission= dateEmission
        self.dateReservation = dateReservation
        self.datePaiement = datePaiement

    def getClient(self):
        """
        Method to retrieve the client associated with the ticket.

        Returns:
            str: Client associated with the ticket
        """
        return self.client

    def getVol(self):
        """
        Method to retrieve the flight associated with the ticket.

        Returns:
            str: Flight associated with the ticket
        """
        return self.vol

    def getNumBillet(self):
        """
        Method to retrieve the number of the ticket.

        Returns:
            int: Number of the ticket
        """
        return self.numBillet

    def getDateEmission(self):
        """
        Method to retrieve the date of ticket emission.

        Returns:
            str: Date of ticket emission
        """
        return self.dateEmission

    def getDateReservation(self):
        """
        Method to retrieve the date of ticket reservation.

        Returns:
            str: Date of ticket reservation
        """
        return self.dateReservation

    def getDatePaiement(self):
        """
        Method to retrieve the date of ticket payment.

        Returns:
            str: Date of ticket payment
        """
        return self.datePaiement

    def __str__(self):
        """
        Method to return a string representation of the BilletModel object.

        Returns:
            str: String representation of the BilletModel object
        """
        return (f"Billet -> ["
                f"Client: {self.client}, "
                f"Vol: {self.vol}, "
                f"NumBillet: {self.numBillet}, "
                f"DateEmission: {self.dateEmission}, "
                f"DateReservation: {self.dateReservation}, "
                f"DatePaiement: {self.datePaiement} ]")