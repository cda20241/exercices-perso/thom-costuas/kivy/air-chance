"""
Class: ConnexionView

This class represents the view for the login screen. It inherits from the BoxLayout class.

Attributes:
    - controller: The controller associated with the view

Methods:
    - __init__(self, controller, **kwargs): Constructor method to initialize a ConnexionView object
"""

from kivy.uix.boxlayout import BoxLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput
from kivy.uix.button import Button
from kivy.uix.image import Image
from kivy.core.window import Window

from pathlib import Path

from kivy.uix.widget import Widget


class ConnexionView(BoxLayout):

    def __init__(self, controller, **kwargs):
        """
        Constructor method to initialize a ConnexionView object.

        Parameters:
            controller: The controller associated with the view
            **kwargs: Additional keyword arguments
        """
        super(ConnexionView, self).__init__(**kwargs)
        self.controller = controller

        self.orientation = 'vertical'
        Window.size = (600, 400)  # Définir la taille de la fenêtre

        window_width = Window.width
        window_height = Window.height

        self.largeurOccupee = 0.7
        self.largeurWidget = 300
        self.hauteurWidget = 40

        self.add_widget(Widget(size_hint=(None, 1), width=10))

        # Ajout de l'image ou du logo au centre en haut
        imageSrc = Path(__file__).parent / "../Assests/AirChance.png"
        self.image = Image(source=str(imageSrc), size_hint = (2, 2), size = (window_width, window_height), pos_hint = {'center_x': 0.5})
        self.add_widget(self.image)

        # Ajout de l'image ou du logo au centre en haut
        self.label = Label(text="")
        self.add_widget(self.label)

        # Ajout des champs utilisateur et mot de passe
        self.user_input = TextInput(hint_text='Nom d\'utilisateur', multiline=False, size_hint = (self.largeurOccupee, None), size = (self.largeurWidget, self.hauteurWidget), pos_hint = {'center_x': 0.5})
        self.password_input = TextInput(hint_text='Mot de passe', multiline=False, password=True, size_hint=(self.largeurOccupee, None), size=(self.largeurWidget, self.hauteurWidget), pos_hint={'center_x': 0.5})

        self.add_widget(self.user_input)
        self.add_widget(self.password_input)

        # Ajout du bouton "Connexion" et "Inscription"
        self.button_layout = GridLayout(cols=2, size_hint = (self.largeurOccupee, None), size = (self.largeurWidget, self.hauteurWidget), pos_hint = {'center_x': 0.5})

        self.submit_button = Button(text='Connexion', size_hint = (self.largeurOccupee, None), size = (self.largeurWidget, self.hauteurWidget), pos_hint = {'center_x': 0.5})
        self.submit_button.bind(on_press=self.controller.on_submit)

        self.sign_up_button = Button(text='Inscription', size_hint = (self.largeurOccupee, None), size = (self.largeurWidget, self.hauteurWidget), pos_hint = {'center_x': 0.5})
        self.sign_up_button.bind(on_press=self.controller.switch_to_inscription)

        self.button_layout.add_widget(self.sign_up_button)
        self.button_layout.add_widget(self.submit_button)

        self.add_widget(self.button_layout)
        self.add_widget(Widget(size_hint=(None, 1), width=10))



