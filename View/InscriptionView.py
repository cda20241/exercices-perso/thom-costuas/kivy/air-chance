"""
Class: InscriptionView

This class represents the view for the user registration screen. It inherits from the BoxLayout class.

Attributes:
    - controller: The controller associated with the view

Methods:
    - __init__(self, controller, **kwargs): Constructor method to initialize an InscriptionView object
"""

from kivy.uix.boxlayout import BoxLayout
from kivy.uix.dropdown import DropDown
from kivy.uix.button import Button
from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput
from kivy.core.window import Window
from kivy.uix.widget import Widget


class InscriptionView(BoxLayout):

    def __init__(self, controller, **kwargs):
        """
        Constructor method to initialize an InscriptionView object.

        Parameters:
            controller: The controller associated with the view
            **kwargs: Additional keyword arguments
        """
        super(InscriptionView, self).__init__(**kwargs)
        self.controller = controller
        self.orientation = 'vertical'

        Window.size = (600, 400)  # Définir la taille de la fenêtre

        self.largeurOccupee = 0.7
        self.largeurWidget = 300
        self.hauteurWidget = 40

        self.label = Label(text="")
        self.add_widget(self.label)


        # Ajout des champs utilisateur et mot de passe
        self.input_prenom = TextInput(hint_text='Nom', multiline=False, size_hint = (self.largeurOccupee, None), size = (self.largeurWidget, self.hauteurWidget), pos_hint = {'center_x': 0.5})
        self.input_nom = TextInput(hint_text='Prénom', multiline=False, size_hint = (self.largeurOccupee, None), size = (self.largeurWidget, self.hauteurWidget), pos_hint = {'center_x': 0.5})
        self.input_email = TextInput(hint_text='Email', multiline=False, size_hint = (self.largeurOccupee, None), size = (self.largeurWidget, self.hauteurWidget), pos_hint = {'center_x': 0.5})
        self.input_mdp = TextInput(hint_text='Mot de passe', multiline=False, password=True, size_hint = (self.largeurOccupee, None), size = (self.largeurWidget, self.hauteurWidget), pos_hint = {'center_x': 0.5})

        self.add_widget(self.input_prenom)
        self.add_widget(self.input_nom)
        self.add_widget(self.input_email)
        self.add_widget(self.input_mdp)

        self.dropdown = DropDown()

        self.btn = Button(text='Client',size_hint = (self.largeurOccupee, None), size = (self.largeurWidget, self.hauteurWidget), pos_hint = {'center_x': 0.5})
        self.btn.bind(on_release=lambda btn: self.dropdown.select(btn.text))
        self.dropdown.add_widget(self.btn)

        self.btn = Button(text='Controleur',size_hint = (self.largeurOccupee, None), size = (self.largeurWidget, self.hauteurWidget), pos_hint = {'center_x': 0.5})
        self.btn.bind(on_release=lambda btn: self.dropdown.select(btn.text))
        self.dropdown.add_widget(self.btn)

        self.btn_dropdown_menu = Button(text='Choisissez une option',size_hint = (self.largeurOccupee, None), size = (self.largeurWidget, self.hauteurWidget), pos_hint = {'center_x': 0.5})
        self.btn_dropdown_menu.bind(on_release=self.dropdown.open)

        self.dropdown.bind(on_select=lambda instance, x: setattr(self.btn_dropdown_menu, 'text', x))

        self.add_widget(self.btn_dropdown_menu)

        # Ajout du bouton "Connexion" et "Inscription"
        self.button_layout = GridLayout(cols=2, size_hint = (self.largeurOccupee, None), size = (self.largeurWidget, self.hauteurWidget), pos_hint = {'center_x': 0.5})

        self.submit_button = Button(text='Connexion', size_hint = (self.largeurOccupee, None), size = (self.largeurWidget, self.hauteurWidget), pos_hint = {'center_x': 0.5})
        self.submit_button.bind(on_press=self.controller.switch_to_connexion)

        self.sign_up_button = Button(text='Inscription', size_hint = (self.largeurOccupee, None), size = (self.largeurWidget, self.hauteurWidget), pos_hint = {'center_x': 0.5})
        self.sign_up_button.bind(on_press=self.controller.valider_inscription)

        self.button_layout.add_widget(self.sign_up_button)
        self.button_layout.add_widget(self.submit_button)

        self.add_widget(self.button_layout)
        self.add_widget(Widget(size_hint=(None, 1), width=10))


