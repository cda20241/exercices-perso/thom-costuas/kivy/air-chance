"""
Class: AccueilView

This class represents the view for the home page. It inherits from the BoxLayout class.

Attributes:
    - controller: The controller associated with the view

Methods:
    - __init__(self, controller, **kwargs): Constructor method to initialize an AccueilView object
"""

from kivy.uix.boxlayout import BoxLayout
from kivy.uix.label import Label
from kivy.core.window import Window

from View.Controleur.NavBarWidget import NavbarWidget


class AccueilView(BoxLayout):

    def __init__(self, controller, **kwargs):
        """
        Constructor method to initialize an AccueilView object.

        Parameters:
            controller: The controller associated with the view
            **kwargs: Additional keyword arguments
        """
        super(AccueilView, self).__init__(**kwargs)
        self.controller = controller

        self.orientation = 'vertical'

        Window.size = (600, 400)  # Définir la taille de la fenêtre

        navbar_widget = NavbarWidget(controller)
        self.add_widget(navbar_widget)



        # Ajout du message de bienvenue
        self.titre = Label(text="",pos_hint={'top': 1})
        self.add_widget(self.titre)