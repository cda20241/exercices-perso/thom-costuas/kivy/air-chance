"""
Class: ListeVolsView

This class represents the view for displaying a list of flights. It inherits from the BoxLayout class.

Attributes:
    - controller: The controller associated with the view
    - data: Data representing the list of flights
    - filtre: Filter applied to the list of flights

Methods:
    - __init__(self, controller, data, filtre, **kwargs): Constructor method to initialize a ListeVolsView object
    - creerTableau(self, data): Method to create a table based on the given data
"""

from kivy.uix.behaviors import ButtonBehavior
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.label import Label
from kivy.core.window import Window
from kivy.metrics import dp
from kivy.uix.widget import Widget

from View.Controleur.NavBarWidget import NavbarWidget

class ListeVolsView(BoxLayout):
    def __init__(self, controller, data, filtre, **kwargs):
        """
        Constructor method to initialize a ListeVolsView object.

        Parameters:
            controller: The controller associated with the view
            data: Data representing the list of flights
            filtre: Filter applied to the list of flights
            **kwargs: Additional keyword arguments
        """
        super(ListeVolsView, self).__init__(**kwargs)
        self.controller = controller
        self.orientation = 'vertical'
        Window.size = (1000, 800)  # Définir la taille de la fenêtre

        self.largeurWidget = 300
        self.hauteurWidget = 40


        navbar_widget = NavbarWidget(controller)
        self.add_widget(navbar_widget)

        # Ajout du message de bienvenue
        self.titre = Label(text="", pos_hint={'top': 1})
        self.add_widget(self.titre)

        # En-tête du tableau
        self.header_row = BoxLayout(size_hint=(1, 0.8), size=(Window.width, Window.height), pos_hint={'center_x': 0.5}, height=self.hauteurWidget)
        headers = ['Vol', 'Départ', "Arrivée", 'H-Départ', "H-Arrivée", "V-Départ", "V-Arrivée", 'Retard', 'Avion '+filtre,'Option']

        for header_text in headers:
            if "Avion" in header_text:
                header_label = ClickableLabel(text=header_text)
                header_label.bind(on_release=self.controller.change_filter)  # Corrected line
                self.header_row.add_widget(header_label)
            else:
                header_label = Label(text=header_text)
                self.header_row.add_widget(header_label)

        self.add_widget(self.header_row)



    def creerTableau(self,data):
        """
        Method to create a table based on the given data.

        Parameters:
            data: Data representing the list of flights
        """
        for i, row in enumerate(data):
            self.data_row = BoxLayout(size_hint_y=None, height=dp(self.hauteurWidget))

            for key, value in row.items():
                # Create a regular label for other keys
                if key=="NumVol":
                    self.label_numVol = Label(text=str(value), halign='center')
                    self.data_row.add_widget(self.label_numVol)
                else:
                    label_value = Label(text=str(value), halign='center')
                    self.data_row.add_widget(label_value)

            self.modif_button = Button(text='Modifier', size_hint=(1, 1), size=(20, self.hauteurWidget))
            self.modif_button.label = self.label_numVol
            self.modif_button.bind(on_press=lambda instance, label_text=self.label_numVol.text: self.controller.switch_to_modif(label_text))
            self.data_row.add_widget(self.modif_button)

            self.supr_button = Button(text='Annuler', size_hint=(1, 1), size=(20, self.hauteurWidget))
            self.supr_button.label = self.label_numVol
            self.supr_button.bind(on_press=lambda instance, label_text=self.label_numVol.text: self.controller.annuler_vol(label_text))
            self.data_row.add_widget(self.supr_button)


            setattr(self, f"data_row_{i}", self.data_row)
            self.add_widget(self.data_row)

        self.espace = Widget(size_hint=(1, 1), width=10)
        self.add_widget(self.espace)




class ClickableLabel(ButtonBehavior, Label):
    def __init__(self, **kwargs):
        super(ClickableLabel, self).__init__(**kwargs)

    def on_press(self):
        pass
    def on_release(self):
        pass