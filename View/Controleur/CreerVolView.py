"""
Class: CreerVolView

This class represents the view for creating a flight. It inherits from the BoxLayout class.

Attributes:
    - controller: The controller associated with the view
    - dataVol: Data related to the flight
    - dataAvion: Data related to the aircraft

Methods:
    - __init__(self, controller, dataVol, dataAvion, **kwargs): Constructor method to initialize a CreerVolView object
    - select(self, button, value): Method to update the selected aircraft code label
"""

from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.dropdown import DropDown
from kivy.uix.label import Label
from kivy.core.window import Window
from kivy.uix.textinput import TextInput
from kivy.uix.widget import Widget

from View.Controleur.NavBarWidget import NavbarWidget

class CreerVolView(BoxLayout):
    def __init__(self, controller, dataVol, dataAvion, **kwargs):
        """
        Constructor method to initialize a CreerVolView object.

        Parameters:
            controller: The controller associated with the view
            dataVol: Data related to the flight
            dataAvion: Data related to the aircraft
            **kwargs: Additional keyword arguments
        """
        super(CreerVolView, self).__init__(**kwargs)
        self.controller = controller
        self.orientation = 'vertical'
        Window.size = (1000, 800)  # Définir la taille de la fenêtre


        self.largeurOccupee = 0.7
        self.largeurWidget = 300
        self.hauteurWidget = 40


        navbar_widget = NavbarWidget(controller)
        self.add_widget(navbar_widget)

        # Ajout du message de bienvenue
        self.titre = Label(text="", pos_hint={'top': 1})
        self.add_widget(self.titre)

        for key, value in dataVol.items():
            if key!="Avion":
                self.data_row = BoxLayout(size_hint = (self.largeurOccupee, None), size = (self.largeurWidget, self.hauteurWidget), pos_hint = {'center_x': 0.5})
                self.label = Label(text=key+ " : ", size_hint=(0.2, None),size=(self.largeurWidget, self.hauteurWidget), pos_hint={'center_x': 0.5})
                self.input = TextInput(hint_text=key, text="", multiline=False, size_hint=(self.largeurOccupee, None),size=(self.largeurWidget, self.hauteurWidget), pos_hint={'center_x': 0.5})
                setattr(self, f"{key}", self.input)

                self.data_row.add_widget(self.label)
                self.data_row.add_widget(self.input)

                self.add_widget(self.data_row)




        self.dropdown = DropDown()

        self.data_row = BoxLayout(size_hint=(self.largeurOccupee, None), size=(self.largeurWidget, self.hauteurWidget), pos_hint={'center_x': 0.5})
        self.label = Label(text="Avion : ", size_hint=(0.2, None), size=(self.largeurWidget, self.hauteurWidget), pos_hint={'center_x': 0.5})

        self.label_codeAvionSelectionne = Label(text='-1')

        for i, row in enumerate(dataAvion):
            for key, value in row.items():
                if key == "LibelleAvion":
                    self.btn = Button(text=value, size_hint=(self.largeurOccupee, None), size=(self.largeurWidget, self.hauteurWidget), pos_hint={'center_x': 0.5})
                    self.btn.label = self.label_codeAvion
                    self.btn.bind(on_release=lambda btn: self.dropdown.select(btn.text))
                    self.btn.bind(on_release=lambda btn, label_text=self.label_codeAvion.text: self.select(btn, label_text))
                    self.dropdown.add_widget(self.btn)

                elif key == "CodeAvion":
                    self.label_codeAvion = Label(text=value)

        self.btn_dropdown_menu = Button(text="Selectionnez un avion", size_hint=(self.largeurOccupee, None), size=(self.largeurWidget, self.hauteurWidget), pos_hint={'center_x': 0.5})
        self.btn_dropdown_menu.bind(on_release=self.dropdown.open)

        self.dropdown.bind(on_select=lambda instance, x: setattr(self.btn_dropdown_menu, 'text', x))

        self.data_row.add_widget(self.label)
        self.data_row.add_widget(self.btn_dropdown_menu)
        self.add_widget(self.data_row)


        self.add_widget(self.label_codeAvionSelectionne)


        self.espace = Widget(size_hint=(1, 1), width=10)
        self.add_widget(self.espace)

        self.validation_button = Button(text="Confirmer la création", size_hint=(self.largeurOccupee, None), size=(self.largeurWidget, self.hauteurWidget), pos_hint={'center_x': 0.5})
        self.validation_button.bind(on_press=self.controller.ajouterVol)
        self.add_widget(self.validation_button)

        self.espace = Widget(size_hint=(1, 1), width=10)
        self.add_widget(self.espace)

    def select(self, button, value):
        """
        Method to update the selected aircraft code label.

        Parameters:
            button: The button associated with the selection
            value: The value of the selected aircraft code
        """
        self.label_codeAvionSelectionne.text = f'{value}'