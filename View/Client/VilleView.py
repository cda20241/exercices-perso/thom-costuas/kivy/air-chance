"""
Class: VilleView

This class represents the view for displaying information about a city. It inherits from the BoxLayout class.

Attributes:
    - controller: The controller associated with the view

Methods:
    - __init__(self, controller, **kwargs): Constructor method to initialize a VilleView object
"""

from kivy.uix.boxlayout import BoxLayout
from kivy.uix.label import Label
from kivy.core.window import Window

from View.Client.NavBarWidget import NavbarWidget

class VilleView(BoxLayout):
    def __init__(self, controller, **kwargs):
        """
        Constructor method to initialize a VilleView object.

        Parameters:
            controller: The controller associated with the view
            **kwargs: Additional keyword arguments
        """
        super(VilleView, self).__init__(**kwargs)
        self.controller = controller
        self.orientation = 'vertical'
        Window.size = (600, 400)

        navbar_widget = NavbarWidget(controller)
        self.add_widget(navbar_widget)

        # Ajout du message de bienvenue
        self.titre = Label(text="", pos_hint={'top': 1})
        self.add_widget(self.titre)
