"""
Class: ListeReservationView

This class represents the view for displaying a list of reservations. It inherits from the BoxLayout class and contains a navigation bar widget, a welcome message label, and a table-like layout to display reservation data.

Attributes:
    - controller: The controller associated with the view
    - data: Data to be displayed in the reservation list

Methods:
    - __init__(self, controller, data, **kwargs): Constructor method to initialize a ListeReservationView object
"""

from kivy.metrics import dp
from kivy.uix.boxlayout import BoxLayout
from kivy.core.window import Window
from kivy.uix.label import Label

from kivy.uix.widget import Widget

from View.Client.NavBarWidget import NavbarWidget


class ListeReservationView(BoxLayout):

    def __init__(self, controller, data, **kwargs):
        """
        Constructor method to initialize a ListeReservationView object.

        Parameters:
            controller: The controller associated with the view
            data: Data to be displayed in the reservation list
            **kwargs: Additional keyword arguments
        """
        super(ListeReservationView, self).__init__(**kwargs)
        self.controller = controller

        self.orientation = 'vertical'

        Window.size = (1000, 800)  # Définir la taille de la fenêtre
        window_width = Window.width
        window_height = Window.height

        self.largeurWidget = 300
        self.hauteurWidget = 40


        navbar_widget = NavbarWidget(controller)
        self.add_widget(navbar_widget)

        # Ajout du message de bienvenue
        self.titre = Label(text="", pos_hint={'top': 1})
        self.add_widget(self.titre)

        headers = ['Client', 'Vol', "NumBillet", "Date d'émission", "Date de réservation", "Date de paiement"]

        self.header_row = BoxLayout(    size_hint=(1, 0.8), size=(Window.width, Window.height), pos_hint={'center_x': 0.5}, height=self.hauteurWidget)

        for header_text in headers:
            header_label = Label(text=header_text)
            self.header_row.add_widget(header_label)
        self.add_widget(self.header_row)

        # On crée les lignes dynamiquement
        for row in data:
            data_row = BoxLayout(size_hint_y=None, height=dp(self.hauteurWidget))
            for key, value in row.items():
                data_row.add_widget(Label(text=f'{value}'))
            self.add_widget(data_row)

        self.add_widget(Widget(size_hint=(1, 1), width=10))