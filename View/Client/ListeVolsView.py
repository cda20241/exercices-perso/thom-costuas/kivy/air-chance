"""
Class: ListeVolsView

This class represents the view for displaying a list of flights. It inherits from the BoxLayout class and contains a navigation bar widget, a welcome message label, and a table-like layout to display flight data.

Attributes:
    - controller: The controller associated with the view
    - data: Data to be displayed in the flight list

Methods:
    - __init__(self, controller, data, **kwargs): Constructor method to initialize a ListeVolsView object
"""

from kivy.uix.behaviors import ButtonBehavior
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.label import Label
from kivy.core.window import Window
from kivy.metrics import dp
from kivy.uix.widget import Widget

from View.Client.NavBarWidget import NavbarWidget

class ListeVolsView(BoxLayout):
    def __init__(self, controller, data, **kwargs):
        """
        Constructor method to initialize a ListeVolsView object.

        Parameters:
            controller: The controller associated with the view
            data: Data to be displayed in the flight list
            **kwargs: Additional keyword arguments
        """
        super(ListeVolsView, self).__init__(**kwargs)
        self.controller = controller
        self.orientation = 'vertical'
        Window.size = (1000, 800)  # Définir la taille de la fenêtre

        self.largeurWidget = 300
        self.hauteurWidget = 40

        navbar_widget = NavbarWidget(controller)
        self.add_widget(navbar_widget)

        # Ajout du message de bienvenue
        self.titre = Label(text="", pos_hint={'top': 1})
        self.add_widget(self.titre)

        # En-tête du tableau
        self.header_row = BoxLayout( size_hint=(1, 0.8), size=(Window.width, Window.height), pos_hint={'center_x': 0.5}, height=self.hauteurWidget)
        headers = ['Vol', 'Date de départ', "Date d'arrivée", 'Heure de départ', "Heure d'arrivée", "Ville de départ",
                   "Ville d'arrivée", 'Retard', 'Avion']

        self.header_row = BoxLayout(    size_hint=(1, 0.8), size=(Window.width, Window.height), pos_hint={'center_x': 0.5}, height=self.hauteurWidget)

        for header_text in headers:
            header_label = Label(text=header_text)
            self.header_row.add_widget(header_label)
        self.add_widget(self.header_row)

        # On crée les lignes dynamiquement
        for row in data:
            data_row = BoxLayout(size_hint_y=None, height=dp(self.hauteurWidget))
            for key, value in row.items():
                # Check if the key is VilleDepart, VilleArrivee, or NumVol
                if key in ["VilleDepart", "VilleArrivee", "NumVol"]:
                    # Create a clickable label with underlined text
                    label_value = ClickableLabel(text=f"[u]{value}[/u]", markup=True)
                    # Bind the appropriate event based on the key
                    if key == "VilleDepart" or key == "VilleArrivee":
                        label_value.bind(on_release=self.controller.switch_to_ville)
                    elif key == "NumVol":
                        label_value.bind(on_release=self.controller.switch_to_reservation)
                else:
                    # Create a regular label for other keys
                    label_value = Label(text=str(value))
                data_row.add_widget(label_value)

            self.add_widget(data_row)

        self.add_widget(Widget(size_hint=(1, 1), width=10))




class ClickableLabel(ButtonBehavior, Label):
    def __init__(self, **kwargs):
        super(ClickableLabel, self).__init__(**kwargs)
        """
        Constructor method to initialize a ClickableLabel object.

        Parameters:
            **kwargs: Additional keyword arguments
        """

    def on_press(self):
        """
        Event handler for the press event.
        """
        pass
    def on_release(self):
        """
        Event handler for the release event.
        """
        pass
