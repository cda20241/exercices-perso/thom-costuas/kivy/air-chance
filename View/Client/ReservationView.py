"""
Class: ReservationView

This class represents the view for making a reservation. It inherits from the BoxLayout class.

Attributes:
    - controller: The controller associated with the view
    - data: Data related to the reservation
    - estDejaReserve: Flag indicating whether the reservation has already been made

Methods:
    - __init__(self, controller, data, estDejaReserve, **kwargs): Constructor method to initialize a ReservationView object
"""

from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.label import Label
from kivy.core.window import Window
from kivy.metrics import dp
from kivy.uix.widget import Widget

from View.Client.NavBarWidget import NavbarWidget

class ReservationView(BoxLayout):
    def __init__(self, controller, data, estDejaReserve, **kwargs):
        """
        Constructor method to initialize a ReservationView object.

        Parameters:
            controller: The controller associated with the view
            data: Data related to the reservation
            estDejaReserve: Flag indicating whether the reservation has already been made
            **kwargs: Additional keyword arguments
        """
        super(ReservationView, self).__init__(**kwargs)
        self.controller = controller
        self.orientation = 'vertical'
        Window.size = (1000, 800)  # Définir la taille de la fenêtre

        self.largeurBouton = 300
        self.hauteurBouton = 40


        navbar_widget = NavbarWidget(controller)
        self.add_widget(navbar_widget)

        # Ajout du message de bienvenue
        self.titre = Label(text="", pos_hint={'top': 1})
        self.add_widget(self.titre)

        # En-tête du tableau
        self.header_row = BoxLayout(size_hint=(1, 0.8), size=(Window.width, Window.height), pos_hint={'center_x': 0.5}, height=self.hauteurBouton)
        headers = ['Vol', 'Date de départ', "Date d'arrivée", 'Heure de départ', "Heure d'arrivée", "Ville de départ", "Ville d'arrivée", 'Retard', 'Avion']

        self.header_row = BoxLayout(size_hint=(1, 0.8), size=(Window.width, Window.height), pos_hint={'center_x': 0.5}, height=self.hauteurBouton)

        for header_text in headers:
            header_label = Label(text=header_text)
            self.header_row.add_widget(header_label)
        self.add_widget(self.header_row)

        # On crée les lignes dynamiquement
        for row in data:
            data_row = BoxLayout(size_hint_y=None, height=dp(self.hauteurBouton))
            for key, value in row.items():
                # Create a regular label for other keys
                label_value = Label(text=str(value))
                data_row.add_widget(label_value)
            self.add_widget(data_row)
        self.add_widget(Widget(size_hint=(1, 1), width=10))


        if estDejaReserve:
            self.reservation_button = Button(text='Vous avez déjà réservé ce vol !', size_hint=(None, None), size=(self.largeurBouton, self.hauteurBouton), pos_hint={'center_x': 0.5})
        else:
            self.reservation_button = Button(text='Procéder à la réservation', size_hint=(None, None), size=(self.largeurBouton, self.hauteurBouton), pos_hint={'center_x': 0.5})
            self.reservation_button.bind(on_press=self.controller.reserver)
        self.add_widget(self.reservation_button)

        self.add_widget(Widget(size_hint=(1, 1), width=10))
