"""
Class: NavbarWidget

This class represents the navigation bar widget containing various buttons for navigation purposes. It inherits from the BoxLayout class.

Attributes:
    - controller: The controller associated with the widget

Methods:
    - __init__(self, controller, **kwargs): Constructor method to initialize a NavbarWidget object
"""

from kivy.uix.boxlayout import BoxLayout
from kivy.uix.dropdown import DropDown
from kivy.uix.button import Button
from kivy.uix.image import Image
from kivy.core.window import Window

from pathlib import Path


class NavbarWidget(BoxLayout):

    def __init__(self, controller, **kwargs):
        """
        Constructor method to initialize a NavbarWidget object.

        Parameters:
            controller: The controller associated with the widget
            **kwargs: Additional keyword arguments
        """
        super(NavbarWidget, self).__init__(**kwargs)
        self.controller = controller
        self.orientation = 'horizontal'

        self.size_hint_y = None
        self.height = 60

        window_width = Window.width
        window_height = Window.height

        self.largeurBoutonNav = window_width / 4 - 5
        self.hauteurBoutonNav = 40


        # Ajout du logo
        self.imageSrc = Path(__file__).parent / "../../Assests/AirChance.png"
        self.logo = Image(source=str(self.imageSrc), size_hint=(None, 1), width=100)


        # Ajout des boutons
        self.accueil_button = Button(text='Accueil', size_hint=(1, 1), size=(self.largeurBoutonNav, self.hauteurBoutonNav), pos_hint={'center_x': 0.5})
        self.accueil_button.bind(on_press=self.controller.switch_to_accueil)

        self.dropdown = DropDown()

        self.btn = Button(text='Passées', size_hint=(1, None), size=(self.largeurBoutonNav, self.hauteurBoutonNav), pos_hint={'center_x': 0.5})
        self.btn.bind(on_press=self.controller.switch_to_listeReservations_passee, on_release=lambda btn: self.dropdown.select(self.btn_dropdown_menu.text))
        self.dropdown.add_widget(self.btn)

        self.btn = Button(text='A venir', size_hint=(1, None), size=(self.largeurBoutonNav, self.hauteurBoutonNav), pos_hint={'center_x': 0.5})
        self.btn.bind(on_press=self.controller.switch_to_listeReservations_future, on_release=lambda btn: self.dropdown.select(self.btn_dropdown_menu.text))
        self.dropdown.add_widget(self.btn)

        self.btn_dropdown_menu = Button(text='Réservations',  size_hint=(1, 1), size=(self.largeurBoutonNav, self.hauteurBoutonNav), pos_hint={'center_x': 0.5})
        self.btn_dropdown_menu.bind(on_release=self.dropdown.open)

        self.dropdown.bind(on_select=lambda instance, x: setattr(self.btn_dropdown_menu, 'text', x))

        self.listeVols_button = Button(text='Liste des vols', size_hint=(1, 1), size=(self.largeurBoutonNav, self.hauteurBoutonNav), pos_hint={'center_x': 0.5})
        self.listeVols_button.bind(on_press=self.controller.switch_to_listeVols)

        self.disconnect_button = Button(text='Déconnexion', size_hint=(1, 1), size=(self.largeurBoutonNav, self.hauteurBoutonNav), pos_hint={'center_x': 0.5})
        self.disconnect_button.bind(on_press=self.controller.switch_to_connexion)

        self.add_widget(self.accueil_button)
        self.add_widget(self.btn_dropdown_menu)
        self.add_widget(self.listeVols_button)
        self.add_widget(self.disconnect_button)
