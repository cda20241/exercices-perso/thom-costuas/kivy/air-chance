"""
Main Application File: main.py

This file contains the main application logic for the ConnexionApp.

Classes:
    - ConnexionApp: The main application class responsible for setting up and running the ConnexionApp.

Functions:
    - build(): Method to build the application and return the view associated with the ConnexionController.

Execution Flow:
    - The ConnexionApp is instantiated and run when the file is executed.

Usage:
    - Run this file to start the ConnexionApp.

"""

# main.py
from kivy.app import App
from kivy.core.window import Window

from Controller.ConnexionController import ConnexionController


class ConnexionApp(App):
    """
    ConnexionApp class.

    This class represents the main application. It initializes the ConnexionController and sets up the window.

    Methods:
        - build(self): Method to build the application and return the view associated with the ConnexionController.
    """
    def build(self):
        """
        Method to build the application and return the view associated with the ConnexionController.

        Returns:
            self.connexion_controller.view: The view associated with the ConnexionController.
        """
        self.connexion_controller = ConnexionController()

        Window.set_title("Connexion")
        return self.connexion_controller.view

if __name__ == "__main__":
    ConnexionApp().run()




# | FAIT | - Connexion (Pour les contrôleurs et les clients)
# | FAIT | - Inscription (Pour les contrôleurs et les clients)
# | FAIT | - Déconnexion

# Espace agent de la compagnie :
#
# | FAIT | - Importer une liste de comptes au format CSV
# | FAIT | - Afficher la liste de tous les vols à venir triés du départ le plus proche dans le temps, au départ le plus loin
# | FAIT | - Afficher la liste de tous les vols passés triés du plus proche dans le temps, au plus loin
# | FAIT | - Créer des vols
# | FAIT | - Éditer des vols
# | FAIT | - Annuler des vols
# | FAIT | - Un agent peut annuler une réservation d'un client
# | FAIT / BUG | - Sur les listes des vols, avoir un filtre sur le modele d'avion
# | PAS FAIT | Quand on annule un vol, cela ne le supprime pas il reste enregistré mais dans l'état annulé

# Espace client :
#
# | FAIT | - Avoir un espace accueil avec la liste de tous les prochains vols réservés
# | FAIT | - Visualiser tous les vols passés que le client a réservé
# | FAIT | - Visualiser tous les vols à venir
# | FAIT | - Reserve un vol


# Open Weather Map
# La clé API fonctionne
# | FAIT | - Afficher sur l'espace accueil la météo du jour de la position du client (utiliser une API météo)
# | FAIT | - Afficher la météo de la ville de départ et d'arrivée de chaque vol, pour la date de départ prévue
